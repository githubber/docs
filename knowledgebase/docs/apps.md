# Apps

## Installation

Apps can be installed from the `App Store` menu item. Clicking on an app will display
information about the app.

<center>
<img src="/img/app_info.png" class="shadow" width="500px">
</center>

<br/>

Clicking the `Install` button will show an install dialog:

<center>
<img src="/img/app_install.png" class="shadow" width="500px">
</center>

<br/>

The `Location` field is the subdomain into which the app will be installed. Use the drop down
selector on the right to choose the domain into which the app will by installed.
If the subdomain field is empty, the app will be installed in the bare/naked domain (i.e
at `smartserver.space` in the picture above).

!!! note "Location field can be multi-level"
    The `Location` field can be any level deep. For example, you can specify location as
    `blog.dev` to make the app available at `blog.dev.smartserver.space`.

## Configuration

Clicking on the gear button will open the app's configure view.

<center>
<img src="/img/app-configure-button.png" class="shadow" width="250px">
</center>

## Location

Changing the location field in the `Location` section of the app's configure UI will move the app to
another domain or subdomain:

<center>
<img src="/img/app-location.png" class="shadow" width="500px">
</center>

!!! note "No data loss"
    Moving an app to a new location is a non-destructive action. Existing app data will
    be migrated to the new domain.

## Aliases

Aliases can be setup from the `Location` section in the app's configure UI:

<center>
<img src="/img/app-aliases.png" class="shadow" width="500px">
</center>

The alias feature is only enabled for select apps since it requires apps to support multiple
domains. Notably apps like WordPress, EspoCRM, Surfer support this feature.

## Redirections

Redirections can be setup from the `Location` section in the app's configure UI:

<center>
<img src="/img/app-redirect.png" class="shadow" width="500px">
</center>

In the above example, anyone visiting `chat2.cloudron.ml` or `chat3.smartserver.io` will be automatically
redirected to the main domain `chat.cloudron.ml` (using HTTP 302).

The redirection feature preserves any URI components like subpaths in the original request.

## Labels and Tags

`Label` is the text that is displayed for the app on the dashboard below the icon. `Tags` are a
mechanism to tag apps with labels. For example, you can mark specific apps with the customer name
and filter apps by customer name.

The label and tag of an app can be changed in the `Display` section of the app's configure UI:

<center>
<img src="/img/app-labels-and-tags.png" class="shadow" width="500px">
</center>

## Memory limit

All apps are run with a memory limit to ensure that no app can bring down the whole
Cloudron. The default memory limit of an app is set by the app author at packaging
time. This limit is usually the minimum amount of memory required for the app.
Cloudron admins are expected to tweak the memory limit of an app based on their usage.

When an app runs out of memory, Cloudron automatically restarts it and sends an OOM
email notification to Cloudron admins.

The memory limit can be set by adjusting the slider in the `Resources` section
of the app's configure view.

<center>
<img src="/img/app-memory-slider.png" class="shadow" width="500px">
</center>

!!! note "Memory limit includes swap"
    The memory limit specified above is a combination of RAM and swap space to be allocated
    for the app. Cloudron allocates 50% of this value as RAM and 50% as swap.

## CPU Shares

By default, all apps use as much CPU as they need. When CPU cycles are constrained, the
CPU share setting determines how the linux kernel should share CPU usage across apps.
The CPU share setting is a percentage relative to each app. It priorotizes CPU
resources for the available CPU cycles. It does not reserve specific CPU access.

The CPU share can be set by adjusting the slider in the `Resources` section
of the app's configure view.

<center>
<img src="/img/app-cpu-share.png" class="shadow" width="500px">
</center>


## Access Restriction

Many apps in Cloudron are integrated with Cloudron's user management. For such
apps, one or more groups or users can be assigned to an app to restrict
login. For apps not integrated with Cloudron user management, see the section
on controlling the [visibility of app icon in dashboard](#visibility-of-app-icon-in-dashboard).

Note that Cloudron only handles authentication. Assigning roles to users is
done within the application itself. For example, changing a user to become a
`commenter` or `author` inside WordPress has to be done within WordPress.

<center>
<img src="/img/app-configure-group-acl.png" class="shadow" width="500px">
</center>

* `Allow all users from this Cloudron` - Any user in the Cloudron can access the app.
* `Only allow the following users and groups` - Only the users and groups can access the app.

## Dashboard Visibility

The Dashboard of a Cloudron user displays the apps that the user can access. For apps that
use Cloudron Single Sign-on, the dashboard only displays an app if the user
[has access to it](#restricting-app-access-to-specific-users).

For apps configured to not use the Cloudron Single Sign-on (for example, some public app like a
Forum or Chat), the apps are displayed (by default) on the dashboard of all users. Admins
can control if an app appears in a user's dashboard using the `Dashboard Visibility` section
in the app's configure UI.

<center>
<img src="/img/apps-dashboard-visibility.png" class="shadow" width="500px">
</center>

## Mounts

Apps on Cloudron are containerized and do not have access to the server's file system. To
provide an app access to a path on the server, one can create a [Volume](/storage/#volumes) and then mount the
volume into the app.

First, create a volume in the `Volumes` view:

<center>
<img src="/img/storage-volumes-add.png" class="shadow" width="500px">
</center>

Then, the volume can be mounted into one or more apps.

<center>
<img src="/img/apps-mount.png" class="shadow" width="500px">
</center>

Volumes are mounted under the `/media` directory of the app. In the example above, the app
can access music files from `/media/music` (which corresponds to the host path `/mnt/songs`).

When the read only flag is checked, the `/media/music` directory is not writable.

## Mail FROM address

For apps that can send and/or receive email, Cloudron automatically assigns an address of the
form `<location>.app`. To change this name, go to the `Email` section in the app's configure UI.

<center>
<img src="/img/apps-mailbox-name.png" class="shadow" width="500px">
</center>

!!! note "Mailbox is not automatically created"
    The app is simply configured to send mails with the above name. If you want to receive
    email with the address, be sure to [create a mailbox](/email/#creating-mailboxes)
    or assign it as an [alias](/email/#creating-mail-aliases) for an existing mailbox.
    If a mailbox with the name does not exist, any replies to the email will bounce.

## robots.txt

The `Robots.txt` file is a file served from the root of a website to indicate which parts must be indexed by a search
engine. The file follows the [Robots Exclusion Standard](https://en.wikipedia.org/wiki/Robots_exclusion_standard).
Google has an [excellent document](https://developers.google.com/search/reference/robots_txt) about the semantics.

The robots.txt contents of an app can be set in the `Security` section of the app's configure UI.

By default, Cloudron does not setup a robots.txt for apps. When unset, the app is free to provide it's own robots.txt.

<center>
<img src="/img/app-robots-txt.png" class="shadow" width="500px">
</center>

In addition, the Cloudron admin page has a hardcoded robots.txt that disables indexing:
```
User-agent: *
Disallow: /
```

## Custom CSP

The CSP HTTP header instructs the browser to only load scripts, media, images and other resources only from specific
sites. Some apps set these headers to be overly restrictive and provide no way to customize them. For such apps,
you can override the CSP headers set by the app.

For example, to [embed Mattermost](https://github.com/mattermost/docs/blob/master/source/integrations/embedding.rst)
in another site, you can set the following CSP policy for Mattermost:

```
frame-ancestors site.example.com;
```

<center>
<img src="/img/app-security-csp.png" class="shadow" width="500px">
</center>

## Web terminal

Cloudron provides a web terminal that gives access to the app's file system. The web terminal
can be used to introspect and modify the app's files, access the app's database etc. Note that 
Cloudron runs apps as containers with a read-only file system. Only `/run` (dynamic data),
`/app/data` (backup data) and `/tmp` (temporary files) are writable.

The web terminal can be accessed using the Web Terminal button:

<center>
<img src="/img/app-terminal-button.png" class="shadow" width="400px">
</center>

Clicking the icon will pop up a new window. The terminal is essentially a shell into the app's file system.

<center>
<img src="/img/terminal-exec2.png" class="shadow" width="500px">
</center>

## File manager

Cloudron provides a File Manager that be used to modify the app's file system from the browser.

The File Manager can be accessed using the File Manager button:

<center>
<img src="/img/app-filemanager-button.png" class="shadow" width="400px">
</center>

Clicking the icon will pop up a new window. Note that there are action like Rename, Delete, Change Ownership
in the context menu.

<center>
<img src="/img/filemanager.png" class="shadow" width="500px">
</center>

## FTP Access

Certain apps like WordPress, LAMP, Surfer support access to their data via SFTP. Files can be viewed
and uploaded using any SFTP client. The FTP connection information is displayed in the information
`Access Control` section of the app's configure UI.

<center>
<img src="/img/app-sftp-info.png" class="shadow" width="500px">
</center>

A SFTP client like [FileZilla](https://filezilla-project.org/) can be used to connect as follows:

* `Host` - `sftp://my.cloudron.ml`
* `Username` - `girish@blog2.cloudron.ml`
* `Password` - Cloudron password
* `Port` - 222

<center>
<img src="/img/sftp-filezilla.png" class="shadow" width="500px">
</center>

!!! note "Port 222"
    SFTP service runs at port 222. The server firewall already has this port open. However, you will
    have to whitelist this port in the Cloud firewall (e.g EC2 Security Group or DigitalOcean Firewall).
    If the domain is fronted by Cloudflare, use the IP address of the server to connect via SFTP instead
    of `my.domain.com`.

### Non admin access

By default, only Cloudron admins have SFTP access. Before giving SFTP access to non-admins especially
for apps like WordPress, one should be aware that since the code is modifiable, a non-admin can potentially
write code to log the passwords of an admin user and thus compromising the system. If this is not a problem,
you can provide SFTP access to non-admin from the `Services` view -> `SFTP` -> `Configure`.

<center>
<img src="/img/apps-sftp-non-admin.png" class="shadow" width="500px">
</center>


## Log viewer

To view the logs of an app, click the logs button:

<center>
<img src="/img/app-logs-button.png" class="shadow" width="500px">
</center>

This will open up a popup dialog that display the logs:

<center>
<img src="/img/app-logs.png" class="shadow" width="500px">
</center>

## Staging environment

When doing website development, it is useful to have production and staging environments. Cloudron's
backup and clone features can be used to create a workflow as follows:

* Install the app in `staging.example.com`. Do edits and development as desired.
* Once ready, make a 'snapshot' of the app by making an [app backup](/backups/#making-an-app-backup)
* Use the [clone UI](/backups/#clone-app) to install the app
  into `prod.example.com`.

The same mechanism above can be used bring latest production data to staging.

## Graphs

The Graphs view shows an overview of the disk and memory usage on your Cloudron.

<img src="/img/graphs.png" class="shadow">

The `Disk Usage` graph shows you how much disk space you have left. Note that the Cloudron will
send the Cloudron admins an email notification when the disk is ~90% full.

The `Apps` Memory graph shows the memory consumed by each installed app. You can click on each segment
on the graph to see the memory consumption over time in the chart below it.

The `System` Memory graph shows the overall memory consumption on the entire Cloudron. If you see
the Free memory < 50MB frequently, you should consider upgrading to a Cloudron with more memory.

## Stop app

An app can be stopped using the Stop button from the app toolbar.

<center>
<img src="/img/app-stop-button.png" class="shadow" width="500px">
</center>

## Uninstall

An app can be uninstalled clicking the `Uninstall` button in the app's configure UI.

<center>
<img src="/img/app-uninstall-button.png" class="shadow" width="500px">
</center>

Uninstalling an app immediately removes all data associated with the app from the Cloudron.

!!! note "Backups are not removed"
    App backups are not removed when it is uninstalled and are only cleaned up based on the backup
    policy. Apps can always be [re-instated](/backups/#restoring-an-app-from-existing-backup)
    from their backups using the CLI tool.

