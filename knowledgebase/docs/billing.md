# Pricing & Billing

## Form of payments

We accept Visa, MasterCard, American Express and Discover.

We do not store any credit card information and all payments are handled through [Stripe](https://stripe.com).

## Billing period

We charge on a monthly or yearly subscription basis (prepaid). Any charges will be made on the first day
of the billing period. Any service can be cancelled at any time, the service will then
be terminated at the end of the current billing period.

## Canceling subscription

Subscription can be cancelled anytime at [cloudron.io](/console.html#/userprofile?view=subscriptions).
Once cancelled, we will stop billing you from the next month.

## VAT & Taxes

For customers in the EU, VAT (value added taxes) is NOT included in the price. The VAT amount depends
on the rate of the country of your billing address. Customers outside the EU won't have any taxes applied.

## Credit Card Hold & Duration

When you enter a card, we verify the card through our external payment provider stripe.
We will attempt a small hold on your card. This hold will get removed after 7 days.

## Payment failures

Payment failures can be resolved by adding a <a href="https://cloudron.io/console.html#/userprofile?view=credit_card" target="_blank">new credit card</a>
to your account. As we only allow one credit card per account, you will need to overwrite the old credit card information in your
account page. Our billing system will automatically retry using any new credit card.

## Invoices

Invoices can be download from <a href="https://cloudron.io/console.html#/userprofile?view=invoice_list" target="_blank">here</a>.

