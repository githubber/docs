# Certificates

## Overview

Cloudron integrates with [Let's Encrypt](http://letsencrypt.org/) to install certificates
for apps. Certificates are renewed automatically.

## Certificate Providers

Cloudron supports the following certificate providers:

* `Let's Encrypt Prod` - Obtain certs individually for each domain
* `Let's Encrypt Prod - Wildcard` (default) - Obtain wildcard certs for each domain
* `Let's Encrypt Staging` - Obtain certs individually for each domain from Let's Encrypt [staging endpoint](https://letsencrypt.org/docs/staging-environment/). These certs are for testing and not trusted by the browser.
* `Let's Encrypt Staging - Wildcard` - Obtain wildcard certs for each domain from Let's Encrypt [staging endpoint](https://letsencrypt.org/docs/staging-environment/). These certs are for testing and not trusted by the browser.
* `Custom Wildcard Certificate` - Disable Let's Encrypt integration and use a custom wildcard
  certificate instead.

Certificate provider can be set per-domain from the `Domains` view under the
domain's Advanced settings.

<center>
<img src="/img/certificates-provider.png" class="shadow" width="500px">
</center>

## Custom certificates

### App certificate

To set a custom app certificate, you have to SSH and copy two files under `/home/yellowtent/boxdata/certs`:

* `app.domain.com.user.cert` - the certificate including the full chain
* `app.domain.com.user.key` - they private key

After this, go to the app in Cloudron Dashboard and under `Location`, click Save without making any changes.
This will configure the app to use the new certs.

### Wildcard certificate

A custom wildcard certificate can be provided per domain in advanced settings of a domain in the
`Domains` view. When setting such a certificate, make sure to add both the bare domain and the
wildcard domain as part of the certificate.

Follow [this tutorial](https://stackoverflow.com/questions/1822268/how-do-i-create-my-own-wildcard-certificate-on-linux/1822279)
for instructions on how to generate a custom wildcard certificate that has both the bare domain
and the wildcard domain.

<center>
<img src="/img/certificates-wildcard.png" class="shadow" width="500px">
</center>

!!! note "Intermediate certs"
    You can upload a certificate chain by simply appending all the intermediate certs in
    the same cert file.

### Domain certificate

Custom certificates can also be set for each installed application using the [REST API](/api/#configure-app).
This can be used to set an Extended Validation (EV) certificate for an app. For example,
assuming we have the PEM encoded files `cert.pem` and `key.pem`:

```
# first encode the newlines to send as JSON
key=$(perl -pe 's/\n/\\\n/' key.pem)
cert=$(perl -pe 's/\n/\\n/' cert.pem)

curl -X POST -H "Content-Type: application/json" -d "{ \"cert\": \"${cert}\", \"key\": \"${key}\" }" https://my.cloudron.xyz/api/v1/apps/5555f553-96ad-46c9-ba42-13d08ecb86a0/configure?access_token=3f1e6d8e5ece3f3dbdefd88679fdd270b00223b58ce6781990cf95e444b7c7f3
```

In the example above, `my.example.com` is the Cloudron domain. `5555f553-96ad-46c9-ba42-13d08ecb86a0` is the app id, this
can be obtained by clicking on the `i` button of the app in the dashboard. `access_token` can be obtained from the
user's account page.

!!! note "Intermediate certs"
    You can upload a certificate chain by simply appending all the intermediate certs in
    the same cert file.

## Certificate transparency

Let's Encrypt participates in Certificate transparency. This means that your apps and
subdomains are discoverable via the Certificate transparency project ([crt.sh](https://crt.sh/)
and [Google's website](https://transparencyreport.google.com/https/certificates)). Some 
[hackers](https://www.golem.de/news/certificate-transparency-hacking-web-applications-before-they-are-installed-1707-129172.html)
take advantage of this to hack web applications before they are in installed.

For this reason, we recommend that you use Wildcard certificates. When using Wildcard certificates,
the subdomain information is not 'leaked'. Note that Let's Encrypt only allows obtaining wildcard
certificates using DNS automation. Cloudron will default to obtaining wildcard certificates when
using one of the programmatic [DNS API providers](/domains/#dns-providers).

## Port 80 requirement

Cloudron implements the ACMEv2 API endpoint and can obtain certificates from Let's Encrypt either
via DNS or via HTTP automation.

When using one of the programmatic [DNS API providers](/domains/#dns-providers),
Cloudron will use DNS automation. This means that server's incoming port 80 can be blocked.

When using the Wildcard, Manual or No-op DNS backend, Cloudron will use HTTP automation. This means
that the server's incoming port 80 has to be opened up.

## Fallback certificate

Cloudron generates a self-signed certificate for every domain it manages. This certificate is
used as the fallback if it fails to install or renew Let's Encrypt certificate. The auto-generated
fallback certificate can be replaced with a custom wildcard certificate by editing the domain
in the `Domains` page.

<br/>

<center>
<img src="/img/cert-fallback.png" class="shadow" width="600px">
</center>

## Automatic renewal

Cloudron attempts to start renewing certificates automatically 1 month before expiry of the
certificate. If renewal fails, a notification email will be sent to the Cloudron administrators.
If the Cloudron admin does not take any action (after getting reminded 30 times), Cloudron will start
using [fallback certificates](#fallback-certificate) for the app.

## Manual renewal

To instantly trigger renewal of Let's encrypt certificate, click the `Renew All` button on the domains page.

<center>
<img src="/img/certificates-renew.png" class="shadow" width="600px">
</center>

## Revokation

Cloudron does not revoke certificates when an app is uninstalled. Instead, it retains the
certificate, so that it can be reused if another app is installed in the same
subdomain. This allows you to install apps for testing in the same location, say `test`,
and not have to worry about running over the Let's Encrypt rate limit.

## CAA records

Starting Sep 2017, Let's Encrypt will check for CAA records to validate if the domain owner
has authorized the CA to issue certificates for the domain. For this reason, make sure that
either the CAA record for the domain is [empty](https://community.letsencrypt.org/t/how-to-use-without-caa/38539/2)
OR setup a CAA record allowing `letsencrypt.org`.

## RSA Key Certs

By default, Cloudron uses secp384r1 curve key certs. This should work on most modern browsers and devices (created
in the last 5-6 years).

If you want to change this (for compatibility with old browsers and devices) of a specific app, do the following:

* Install the app to say `app.example.com`.
* Switch to individual certs instead of wildcard certs. You can do this by going to `Domains` -> `Select Domain` -> `Advanced`
  and make sure the certificate provider is `Let's Encrypt Prod`.
* Then, on the server, go to `/home/yellowtent/platformdata/certs`. Move the files `app.example.com.*` to some where else (if they exist)
* Generate the preferred key: `openssl genrsa 4096 > app.example.com.key`. Then `chown yellowtent:yellowtent app.example.com.key`
* Then, go to Domains -> Renew all Certs. This will give the app a cert with the preferred key.

If you want to change the wildcard cert cert itself, do the following:

* Go to `/home/yellowtent/platformdata/certs` and move `_.example.com.*` files to somewhere safe (for the moment).
* Generate the preferred key: `openssl genrsa 4096 > _.example.com.key`. Then `chown yellowtent:yellowtent _.example.com.key`
* Then, go to Domains -> Renew all Certs. This will give all the apps a cert with the preferred key.

