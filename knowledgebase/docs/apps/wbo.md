# <img src="/img/wbo-logo.png" width="25px"> WBO App

## About

WBO is an online collaborative whiteboard that allows many users to draw simultaneously on a large virtual board.

* Questions? Ask in the [Cloudron Forum - WBO](https://forum.cloudron.io/category/116/wbo)
* [WBO Website](https://wbo.ophir.dev/)
* [WBO issue tracker](https://github.com/lovasoa/whitebophir/issues)

