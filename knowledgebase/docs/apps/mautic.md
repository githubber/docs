# <img src="/img/mautic-logo.png" width="25px"> Mautic App

## About

Mautic a free and opensource marketing automation tool.

* Questions? Ask in the [Cloudron Forum - Mautic](https://forum.cloudron.io/category/46/mautic)
* [Mautic Website](https://www.mautic.org/)
* [Mautic forum](https://forum.mautic.org/)
* [Mautic issue tracker](https://github.com/mautic/mautic/issues)

## System cron jobs

Mautic often requires cron jobs to be triggered manually. For this, open the [Web terminal](/apps/#web-terminal)
and run the job manually like so:

```
sudo -E -u www-data php /app/code/bin/console mautic:segments:update
```

You can look into `/app/data/crontab.system` to look into the various pre-configured cron jobs.

## Custom cron jobs

For cron support, edit the file `/app/data/crontab.user` using the the [File manager](/apps/#file-manager).

The crontab contains a line like:

```
0,15,30,45 * * * *  sudo -E -u www-data php /app/code/app/console mautic:integration:fetchleads --integration=Hubspot > /proc/$(cat /var/run/crond.pid)/fd/1 2>&1
```

The app must be restarted after making any changes to the `crontab` file.

## Clearing cache

To clear the cache, you can either delete `/run/mautic/var/cache` or you can run the following command
using the [Web terminal](/apps/#web-terminal):

```
sudo -E -u www-data php /app/code/bin/console cache:clear
```
