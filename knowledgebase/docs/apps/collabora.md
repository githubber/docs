# <img src="/img/collabora-logo.png" width="25px"> Collabora App

## About

Collabora Online is a collaborative online office suite based on LibreOffice technology.

* Questions? Ask in the [Cloudron Forum - Collabora Office](https://forum.cloudron.io/category/57/collabora-office)
* [Collabora Office Website](https://www.collaboraoffice.com)
* [Collabora Office forum](https://forum.collaboraonline.com/)
* [Collabora Office issue tracker](https://github.com/CollaboraOnline/online/issues)

## Setup

The Collabora App can be used to provide rich document editing functionality for
files hosting inside [NextCloud](/apps/nextcloud).

* Install [NextCloud](/store/com.nextcloud.cloudronapp.html) from
  the App Store. For this example, we assume NextCloud was installed at `nextcloud.smartserver.space`.

* Install [Collabora](/store/com.collaboraoffice.coudronapp.html) from the App Store

* In the Collabora setup UI, provide the domain of the NextCloud installation.

  <img src="/img/collabora-settings.png" class="shadow">

* Enable the `Collabora Online` app in NextCloud. This app is under the `Office & text`
  category in the NextCloud app store. Once installed, go to NextCloud `Settings` and
  select the `Collabora Office` item on the left pane. Enter the domain of the collabora
  installation.

  <img src="/img/nextcloud-collabora.png" class="shadow">

* You should now be able to view and edit rich text documents right inside NextCloud.

  <img src="/img/nextcloud-collabora-editor.png" class="shadow">

## Spell check

The empty document templates that are provided by default in Nextcloud are German documents. For
this reason, it might appear that the spell-checker is flagging a lot of spelling errors.

The language of a document can be changed by clicking the menu bar icon on the far right (three
horizontal lines). Then `Tools` -> `Language` -> `For all text`.

<img src="/img/collabora-change-language.png" class="shadow">

