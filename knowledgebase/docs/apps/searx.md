# <img src="/img/searx-logo.png" width="25px"> searx App

## About

Searx is a privacy-respecting metasearch engine.

* Questions? Ask in the [Cloudron Forum - Searx](https://forum.cloudron.io/category/47/searx)
* [Searx Website](https://searx.info)
* [Searx issue tracker](https://github.com/searx/searx/issues)

## Custom configuration

Use the [File Manager](/apps#file-manager) to edit `/app/data/settings.yml`.

