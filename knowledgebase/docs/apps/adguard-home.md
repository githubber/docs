# <img src="/img/adguard-home-logo.png" width="25px"> AdGuard Home App

## About

AdGuard Home is a network-wide software for blocking ads & tracking.

* Questions? Ask in the [Cloudron Forum - AdGuard Home](https://forum.cloudron.io/category/113/adguard-home)
* [AdGuard Home Website](https://adguard.com/en/adguard-home/overview.html)
* [AdGuard Home forum](https://forum.adguard.com/index.php)
* [AdGuard Home issue tracker](https://github.com/AdguardTeam/AdGuardHome/issues)

## Change Password

To change the AdGuard Home password, one must use the `htpasswd` tool.
First, open the [Web terminal](/apps#web-terminal) and run the command below. Note
that the single quote around the password below is not part of the password. It is needed
for the shell to execute the command correctly when your password has special characters.

```
$ htpasswd -nbB admin 'MyNewPassword'
admin:$2y$05$zsr9LdcnDQ3TCBLuyljJHer6XS03ute6GiuA8H7ZjvKuJikud/wk2
```

Copy the password part (after the 'admin:') and put it in `/app/data/AdGuardHome.yaml`
(use the [File Manager](/apps/#file-manager). It's a good idea to quote the password
field. So, it will look like this:

```
users:
- name: admin
  password: "$2y$05$zsr9LdcnDQ3TCBLuyljJHer6XS03ute6GiuA8H7ZjvKuJikud/wk2"
```

The app must be restarted for the password change to take effect.

## Securing Installation

While the admin page is password protected, the DNS server is not. This is because DNS has
no notion of authentication. Leaving your DNS server open will lead to it getting
abused for conducting DDoS reflection and amplification attacks. Many VPS providers
will likely send you a warning/caution email, if you run a open DNS resolver.

We strongly recommend securing your installation in the following ways:

* When available, use your VPS providers firewall functionality to restrict access to
  Port 53 (TCP & UDP).

* In the AdGuard Home dashboard, go to `Settings` -> `DNS settings`. Scroll to the bottom
  for `Access settings` and set a list of clients that can access the DNS server. You
  can also use [ipdeny lists](https://www.ipdeny.com/ipblocks/) to set access and block lists.

    <center>
    <img src="/img/adguard-access-control.png" class="shadow">
    </center>

## DoH

DNS over HTTPS is enabled by default. Cloudron does not support DNS over TLS yet.
Note that there is a Settings page that lets you enable DoH but you won't able to save
that page since this is [not implemented yet](https://github.com/AdguardTeam/AdGuardHome/issues/1009).

This is fine because DoH is enabled in the configs.
