# <img src="/img/discourse-logo.png" width="25px"> Discourse App

## About

Discourse is a platform for community discussion. Free, open, simple. 

* Questions? Ask in the [Cloudron Forum - Discourse](https://forum.cloudron.io/category/33/discourse)
* [Discourse Website](https://www.discourse.org/)
* [Discourse forum](https://meta.discourse.org/)

## Installing plugins

To install a plugin, open a [Web terminal](/apps#web-terminal) and run the following
commands:

```
cd /app/code/plugins
git clone <plugin-repo>
cd /app/code
bundle exec rake plugin:install_gems['REPLACE_WITH_PLUGINS_NAME'] 

# the line below will force assets to be rebuilt on next application restart
rm -rf /run/discourse/public/assets/*
```

Restart the application for the assets to get re-built.

If the plugin modifies the posts in some way, you might want to [rebake posts](#rebaking-posts).

## Changing root account email

The email of the root account is `root@cloudron.local`. Discourse sends an activation email to the old
email address to switch email. Since we don't have access to the default email account, we have to use
the rails console to switch the email.

Open  a [Web terminal](/apps#web-terminal) and run the following commands:

```
# sudo -E -u root bundle exec script/rails console
irb(main):001:0> u = User.find_by_username("root")
irb(main):002:0> u.email = "YOUR_NEW_EMAIL_ADDRESS"
irb(main):003:0> u.email_tokens.create(email: u.email)
irb(main):004:0> u.activate
irb(main):005:0> u.save!
```

## Changing domain

When changing the domain of an existing discourse installation, Cloudron automatically
rebuilds the assets. However, the posts in the forum are not re-written. To rebake
the posts, open a [Web terminal](/apps#web-terminal) and run the following
command:

```
sudo -E -u cloudron bundle exec ruby script/discourse remap old.domain.com new.domain.com
```

## Rebaking posts

To rebuild all posts (for example, to apply formatting provided by a newly installed plugin to
old posts), open a [Web terminal](/apps#web-terminal) and run the following
command:

```
sudo -E -u cloudron bundle exec rake posts:rebake
```

