# <img src="/img/wikijs-logo.png" style="height: 48px; vertical-align: middle;"> Wiki.js App

## About

Wiki.js is a wiki engine running on Node.js

* Questions? Ask in the [Cloudron Forum - Wiki.js](https://forum.cloudron.io/category/51/wiki-js)
* [Wiki.js Website](https://wiki.js.org/)
* [Wiki.js issue tracker](https://github.com/Requarks/wiki/issues)
