# <img src="/img/grav-logo.png" width="25px"> Grav App

## About

Grav is a modern open source flat-file CMS.

* Questions? Ask in the [Cloudron Forum - Grav](https://forum.cloudron.io/category/72/grav-cms)
* [Grav Website](https://getgrav.org)
* [Grav forum](https://discourse.getgrav.org/)
* [Grav issue tracker](https://github.com/getgrav/grav/issues)

## CLI

GPM and Grav commands can be executed by opening a [Web terminal](/apps#web-terminal):

```
# cd /app/code
# sudo -u www-data -- /app/code/bin/gpm install bootstrap4-open-matter
# sudo -u www-data -- /app/code/bin/grav
```

