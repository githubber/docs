# <img src="/img/ghost-logo.png" width="25px"> Ghost App

## About

Ghost makes it simple to publish content online, grow an audience with email newsletters, and make money from premium memberships.

* Questions? Ask in the [Cloudron Forum - Ghost](https://forum.cloudron.io/category/59/ghost)
* [Ghost Website](https://ghost.org/)
* [Ghost forum](https://forum.ghost.org/)
* [Ghost issue tracker](https://github.com/TryGhost/Ghost/issues)

## Structured data

Ghost outputs basic meta tags to allow rich snippets of your content to be recognised by popular social networks.
Currently there are 3 supported rich data protocols which are output in `{{ghost_head}}`:

- Schema.org - http://schema.org/docs/documents.html
- Open Graph - http://ogp.me/
- Twitter cards - https://dev.twitter.com/cards/overview

The Cloudron app enables output of [structured data](https://github.com/TryGhost/Ghost/blob/master/PRIVACY.md#structured-data)
by default.

## Gravatar

For [privacy](https://github.com/TryGhost/Ghost/blob/master/PRIVACY.md) reasons, Gravatar functionality is disabled
by default. You can re-enable this by editing the `useGravatar` field in `/app/data/config.production.json` using
the  [File Manager](/apps#file-manager). Be sure to restart the app after editing the config file.

## Importing

You can import content from another Ghost installation from Settings -> Labs -> Import content.

If the JSON file is large, the import might fail. To fix this:

* Give the app [more memory](/apps/#memory-limit) (say 2GB).
* Next, use the [File Manager](/apps#file-manager) to edit `/app/data/env` to adjust the NodeJS `max-old-space-size` limit.
  Set it to 2GB using a line like this `export NODE_OPTIONS="--max-old-space-size=2048"`
* Restart Ghost and try the import again.

