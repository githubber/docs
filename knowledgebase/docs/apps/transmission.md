# <img src="/img/transmission-logo.png" style="height: 48px; vertical-align: middle;"> Transmission App

## About

Transmission is a fast, easy and free BitTorrent client.

* Questions? Ask in the [Cloudron Forum - Transmission](https://forum.cloudron.io/category/114/transmission)
* [Transmission Website](https://transmissionbt.com/)
* [Transmission forum](https://forum.transmissionbt.com/index.php)
* [Transmission issue tracker](https://github.com/transmission/transmission/issues)

## Paths

By default, this app is configured to, at the end of a download, automatically hard-link the downloaded files from
`/app/data/files/Downloading/` to `/app/data/files/Downloaded/`. This allows potential post-processing by other apps of
the file in `Downloaded`, without interfering with seeding from the file in `Downloading`.

The paths above can be [changed](#custom-config).

## Custom config

Custom configuration can be edited in `/app/data/transmission-config/modifiable.settings.json` using the
[File Manager](/apps/$#file-manager). Be sure to restart the app after making changes.

See [transmission docs](https://github.com/transmission/transmission/wiki/Editing-Configuration-Files) for
a full list of configurable options.

!!! warning "Do not edit settings.json"
    There is a file named `settings.json`. This gets overwritten on startup. Instead, place settings only
    in `modifiable.settings.json`.

