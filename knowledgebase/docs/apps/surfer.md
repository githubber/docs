# <img src="/img/surfer-logo.png" width="25px"> Surfer App

## About

Surfer comes with a webinterface to manage and upload files, a command line tool as well as providing a [WebDAV](https://en.wikipedia.org/wiki/WebDAV) endpoint to manage files in your local file manager.

* Questions? Ask in the [Cloudron Forum - Surfer](https://forum.cloudron.io/category/28/surfer)
* [Surfer Website](https://git.cloudron.io/cloudron/surfer)

## Admin page

The web interface is available under the `https://[appdomain]/_admin/` location.

## CLI tool

Install the surfer cli tool using npm.

```
npm -g install cloudron-surfer
```

Login using your Cloudron credentials:

```
surfer login <this app's domain>
```

Put some files:

```
surfer put index.html favicon.ico /
```

Put a directory (the `/.` below meand that the contents of `build` dir get copied into the root of surfer.
Without it, a `build` directory will get created in the root of surfer).

```
surfer put build/. /
```

## CI/CD integration

You can setup your CI/CD to automatically push static files to surfer as follows:

* First, create an `Access Token` in surfer.

* Install the surfer cli tool as part of the CI/CD pipeline

* Push the artifacts (`dist/` in the example below):

```
surfer put --token api-7e6d90ff-5825-4ebe-a85b-a68795055955 --server surfer.cloudron.ml dist/. /
```

## WebDAV

WebDAV is a well supported extension of the Hypertext Transfer Protocol that allows clients to perform remote Web content authoring operations. WebDAV shares can be mounted usually with your local file manager.

The URI schemes differ on the common platforms:

| Platform| URI |
| ---     | --- |
| Windows | https://[appdomain]/_webdav/ |
| Mac     | https://[appdomain]/_webdav/ |
| Gnome   | davs://[appdomain]/_webdav/ |
| KDE     | webdavs://[appdomain]/_webdav/ |

On Linux the [Davfs2](http://savannah.nongnu.org/projects/davfs2) library can also be used to locally mount a share:
```
mount -t davfs https://[appdomain]/_webdav/ /mount/point
```


## CLI Usage

```
$ surfer
Usage: surfer [options] [command]

Options:
  -V, --version                output the version number
  -s, --server <url>           Server URL (optional)
  -t, --token <access token>   Server Access Token (optional)
  -h, --help                   display help for command

Commands:
  login [options] <url>        Login to server
  logout                       Logout from server
  put [options] <file|dir...>  Puts a list of files or dirs to the destination. The last argument is destination dir
  get [file|dir]               Get a file or directory listing
  del [options] <file>         Delete a file or directory
  help [command]               display help for comman
```

