# <img src="/img/redmine-logo.png" width="25px"> Redmine App

## About

Redmine is a flexible project management web application

* Questions? Ask in the [Cloudron Forum - Redmine](https://forum.cloudron.io/category/52/redmine)
* [Redmine Website](https://redmine.org)
* [Redmine forum](https://redmine.org/projects/redmine/boards)

## Installing plugins

To install plugins in redmine, simply extract them to `/app/data/plugins`
and run the db migration.

* Open a [File manager](/apps#file-manager) for the app.
* Upload and extract the plugin to `/app/data/plugins`

* Some plugins require gems to be installed but for some reason do not have
`source 'https://rubygems.org'` at the start of the Gemfile. If it is missing,
add the line to the top of Gemfile. Then in a [Web terminal](/apps#web-terminal):

```
cd /app/data/plugins/redmine_contacts
vi Gemfile                  # ensure first line is "source 'https://rubygems.org'"
bundle install              # this installs gems into /app/data/bundle/vendor
```

* Initialize the database of the plugin

```
# cd /app/code
# bundle exec rake redmine:plugins NAME=redmine_checklists RAILS_ENV=production
```

* Restart redmine using the `restart` button

## Installing themes

To install plugins in redmine, simply extract them to `/app/data/themes`,
install dependancies and run the build script

```
cd /app/data/themes/
git clone https://github.com/hardpixel/minelab.git
cd minelab
bundle install
./bundle.sh
```

## Code repositories

Redmine can integrate various source code management tools like git, cvs, subversion. The repositories
have to be created manually in `/app/data/repos/` and then configured with that path in the project settings.

For further more detailed information for repository integration can be found [here](http://www.redmine.org/projects/redmine/wiki/RedmineRepositories).

## SSH Keys

Redmine is run as the `cloudron` user. To generate SSH keys for this user, open a
[Web Terminal](/apps#web-terminal) and run the following commands:

```
su - cloudron
ssh-keygen          # generates keys under ~/.ssh. keys are part of the backup
```

## Custom Cron

Custom cron jobs can be placed in the file `/app/data/cron`.

## Custom Settings

The ruby app settings can be overwritten in the file `/app/data/additional_environment.rb`.
For example to put the app in debug mode, add the following to this file and restart the app:
```
config.log_level = :debug
```
