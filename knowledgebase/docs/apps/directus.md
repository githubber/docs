# <img src="/img/directus-logo.png" width="25px"> Directus App

## About

Directus is an Instant App & API for your SQL Database.

* Questions? Ask in the [Cloudron Forum - Directus](https://forum.cloudron.io/category/101/directus)
* [Directus Website](https://directus.io)
* [Directus issue tracker](https://github.com/directus/directus/issues)

## CLI

CLI commands can be executed by opening a [Web terminal](/apps#web-terminal):

```
# cd /app/code
# sudo -u www-data -- /app/code/bin/directus
```

## Multi-tenancy

While Directus supports [multiple projects](https://docs.directus.io/guides/projects.html), the Cloudron package does not.
This is because the Cloudron package is designed with the app having access to only a single database. To create another
project, simply make a new installation of Directus.

