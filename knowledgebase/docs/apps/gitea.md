# <img src="/img/gitea-logo.png" width="25px"> Gitea App

## About

Gitea is a community managed lightweight code hosting solution written in Go.

* Questions? Ask in the [Cloudron Forum - Gitea](https://forum.cloudron.io/category/19/gitea)
* [Gitea Website](https://gitea.io)
* [Gitea forum](https://discourse.gitea.io/)
* [Gitea issue tracker](https://github.com/go-gitea/gitea/issues)

## Customizing Gitea

[Customizing Gitea](https://docs.gitea.io/en-us/customizing-gitea/) is typically done
using the custom folder. This is the central place to override configuration settings,
templates, etc.

On Cloudron, the custom data folder is located at `/app/data/custom`.

Gitea also supports various [configuration customizations](https://docs.gitea.io/en-us/config-cheat-sheet/).
To add customizations, use the [File Manager](/apps#file-manager) and edit
the file named `/app/data/app.ini`.

After editing, restart the app for the changes to take effect.

## LFS

Gitea supports any s3-like storage as a backend for LFS and attachments (see [Gitea Config Cheat Sheet](https://docs.gitea.io/en-us/config-cheat-sheet/#storage-storage)).
Add the following configuration and restarted gitea from the dashboard:

```
[server]
LFS_START_SERVER = true

[storage.my-storage]
STORAGE_TYPE = minio
SERVE_DIRECT = true
MINIO_ENDPOINT = s3.us-west-001.backblazeb2.com 
MINIO_ACCESS_KEY_ID = {secret-id}
MINIO_SECRET_ACCESS_KEY = {secret-key}
MINIO_BUCKET = my-bucket
MINIO_LOCATION = us-west-001
MINIO_USE_SSL = true

[lfs]
STORAGE_TYPE = my-storage

[attachment]
STORAGE_TYPE = my-storage
MAX_SIZE = 50
```

