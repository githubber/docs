# <img src="/img/osticket-logo.png" width="25px"> osTicket App

## About

osTicket is the world’s most popular customer support software.

* Questions? Ask in the [Cloudron Forum - osTicket](https://forum.cloudron.io/category/89/osticket)
* [osTicket Website](https://osticket.com/)
* [osTicket forum](https://forum.osticket.com/)
* [osTicket issue tracker](https://github.com/osTicket/osTicket/issues)

## Admin Checklist

* Do not remove the email address of `osTicket Alerts` under 'Email Addresses'. This mailbox is managed
by Cloudron. However, you can change the email address from Cloudron dashboard's [Email section](/apps/#mail-from-address).

* Change the administrator email under `Emails` -> `Email Settings and Options`. If you miss this,
  osTicket will send alerts to this address and bounces get attached to tickets.

## Emails

osTicket can be configured to process emails from mailboxes hosted with or without Cloudron.

When the mailbox is hosted in Cloudron, you can use the IMAP+SSL at port 993 for receiving Email:

<center>
<img src="/img/osticket-imap.png" class="shadow">
</center>

To send email, use port 587:

<center>
<img src="/img/osticket-smtp.png" class="shadow">
</center>

## User Management

osTicket is integrated with Cloudron user management. However, agents must be manually
added into osTicket before they can login. When adding an agent, choose LDAP as the
authentication backend.

<center>
<img src="/img/osticket-add-agent.png" class="shadow">
</center>

## CLI

osTicket comes with a CLI tool for various administrative tasks like managing users.
Use the [Web Terminal](/apps#web-terminal) to run the following command:

```
    sudo -E -u www-data php /app/code/upload/manage.php
```

