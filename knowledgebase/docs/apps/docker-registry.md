# <img src="/img/docker-registry-logo.png" width="25px"> Docker Registry App

## About

Docker Registry is used for storing and distributing Docker and OCI images using the OCI Distribution Specification.

* Questions? Ask in the [Cloudron Forum - Docker Registry](https://forum.cloudron.io/category/119/docker-registry)
* [Docker Registry docs](https://docs.docker.com/registry/)
* [Docker Registry issue tracker](https://github.com/docker/distribution)

## User Management

## Cloudron Directory

When Cloudron user management is enabled, simply use the Cloudron username and password to login.

## Without Cloudron Directory

When Cloudron user management is disabled, the registry is setup with no authentication. The main use case for this is to
have the registry authenticate with an external provider such as GitLab instead of Cloudron. See the GitLab section
below on how to set this up.

### GitLab Integration

The following steps can be used to setup [GitLab container registry](https://about.gitlab.com/blog/2016/05/23/gitlab-container-registry/).

* Create a [volume](/storage/#volumes) named `registry-shared`.
* Attach volume name `registry-shared` to both GitLab and Docker Registry apps. Be sure to uncheck the `Read Only` checkbox.
* Create folders `containers` and `certs` on the host filesystem inside the path that is assigned to
   the `registry-shared` volume.
* Run the following commands inside the certs folder:

```bash
openssl req -nodes -newkey rsa:2048 -keyout registry-auth.key -out registry-auth.csr -subj "/CN=gitlab-issuer"
openssl x509 -in registry-auth.csr -out registry-auth.crt -req -signkey registry-auth.key -days 365000
chmod 777 registry-auth.key registry-auth.crt registry-auth.csr
```

* Modify `/app/data/config.yml` of the Docker Registry app using the [File manager](/apps/#file-manager) by altering the auth part to resemble the
   following:

```yaml
auth:
  token:
    realm: https://<GITLAB_HOST>/jwt/auth
    service: container_registry
    issuer: gitlab-issuer
    rootcertbundle: /media/registry-shared/certs/registry-auth.crt

```

Change the 'rootdirectory' value inside the same config file to:

```bash
/media/registry-shared/containers
```

Save the file and restart the app.

* Modify `/app/data/gitlab.yml` of the GitLab app by adding the following lines (some of them
   might already be there, so skip them):

```yaml
production:
  <<: *base

  registry:
    enabled: true
    host: <DOCKER_REGISTRY_HOST>
    port: 443
    api_url: https://<DOCKER_REGISTRY_HOST>
    key: /media/registry-shared/certs/registry-auth.key
    path: /media/registry-shared/containers
    issuer: gitlab-issuer
```

Save the file and restart the app.

## Garbage collector

The [Garbage Collector](https://docs.docker.com/registry/garbage-collection/) is automatically run once
a day. You can always trigger an immediate collection using the following command:

```
/usr/local/bin/gosu cloudron:cloudron /app/code/registry garbage-collect /app/data/config.yml
```

