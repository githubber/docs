# <img src="/img/n8n-logo.png" width="25px"> n8n App

## About

n8n Free and open fair-code licensed node based Workflow Automation Tool.

* Questions? Ask in the [Cloudron Forum - n8n](https://forum.cloudron.io/category/129/n8n)
* [n8n Website](https://n8n.io)
* [n8n forum](https://community.n8n.io)
* [n8n issue tracker](https://github.com/n8n-io/n8n/issues)


