# Upgrading to Ubuntu 18.04 (Bionic Beaver)

## Overview

In this guide, we will see how to upgrade an existing Ubuntu 16.04 based Cloudron to Ubuntu 18.04.
Please note that Ubuntu 16.04 will be supported by Canonical till 2021. Cloudron will support
16.04, 18.04 and 20.04. It has the same feature set across all the versions.

## Checklist

Before upgrading, please note the following:

* Cloudron has to be on atleast version 3.3. This can be verified by checking the version in the Settings view. Cloudron releases prior to 3.3 do not support Ubuntu 18.04.
* Ubuntu has to be on version 16.04. Check the output of `lsb_release -a` to confirm this.
* The upgrade takes around 1-3 hours based on various factors like network/cpu/disk etc

## Pre-flight

Before starting the upgrade process, it's a good idea to create a server snapshot to rollback quickly. If your VPS does not have snapshotting feature, it's best to create
a full Cloudron backup before attempting the upgrade (Backups -> Create Backup now).

## Upgrading

Start the upgrade:

```
# dpkg --configure -a
# do-release-upgrade
```

Upgrade notes:

* Accept running an additional ssh deamon at port 1022
* For all packages (nginx, timesyncd, journald etc), select N or O  : keep your currently-installed version. This is the 'default'.
* Accept removal of obsolete packages.
* Restart the server in the end

Finishing the upgrade:

```
# systemctl stop systemd-resolved
# systemctl disable systemd-resolved
# systemctl restart unbound
# systemctl status unbound # this should show 'active (running)'
```

## Post-update checks

`lsb_release -a` will output Ubuntu 18.04.

