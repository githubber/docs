# Migrate existing WordPress installation to Cloudron
  
## Overview

In this guide, we will see two approaches to migrate a WordPress installation from another server or service
to Cloudron. There are no specific limitations of either approach and the method to choose depends on what
tools are avaiable in your existing hosting.

## Migration without a plugin

This approach requires access to a File Manager for downloading site content and PHPMyAdmin for creating a
database dump.

### Export WordPress

The first step is to export the contents of your existing WordPress installation.

* Export the `wp-content/` folder on the origin server to a wp-content.zip file
* Export the database on the source server to a .sql file. In PHPMyAdmin or equivalent, select all the tables, and export to `wordpress.sql`

### Install WordPress on Cloudron

Install the [WordPress Developer](https://www.cloudron.io/store/org.wordpress.unmanaged.cloudronapp.html) app on Cloudron.

### Import WordPress Content

Go to the [file manager](/apps/#file-manager) of the WordPress installation and into the `public/` folder.

* Upload the wp-content.zip file
* Delete existing `wp-content/` folder
* Unzip the file wp-content.zip
* Change ownership to `www-data` from `wp-content/` folder which has been unzipped

### Import WordPress Database

Upload the `wordpress.sql` database dump from the initial step to the path `/app/data`. Go the [Web Terminal](/apps/#web-terminal)
of the app and type the following:

```
# cd /app/data
# wp db reset
# wp db import wordpress.sql
```

### Verify

At this point, the migration is complete and your site should be live. For good measure, restart the app to make sure that
the site works across restarts.

### Replacing URLs

Some WordPress core files, themes and plugins store the entire URL in the database (as opposed to just a relative URL).
The WP CLI tool has a handy `search-replace` subcommand to replace strings in the database.

Open a [Web Terminal](/apps/#web-terminal) and run the command below:

```
# sudo -E -u www-data /app/pkg/wp --path=/app/data/public/ search-replace 'https://old.cloudron.space' 'https://new.cloudron.space'
# sudo -E -u www-data /app/pkg/wp --path=/app/data/public/ cache flush
```

## Migration with All-in-One WP Migration plugin

This approach requires installing the [All-in-One WP Migration](https://wordpress.org/plugins/all-in-one-wp-migration/) plugin
on your existing site.

### Export WordPress

First install the `All-in-One WP Migration` plugin and activate it. Post activation, click on `All-in-One WP Migration` in the
side pane and click Export to file. Download the migration file (has a .press extension).

<img src="/guides/img/wp-migration-all-in-one-export.png" class="shadow" width="500px">

### Install WordPress on Cloudron

Install the [WordPress Developer](https://www.cloudron.io/store/org.wordpress.unmanaged.cloudronapp.html) app on Cloudron.

### Import WordPress Content

Install the `All-in-One WP Migration` plugin on the Cloudron installation and activate it. Post activation, click on `All-in-One WP Migration` in the
side pane and click Import.

<img src="/guides/img/wp-migration-all-in-one-import.png" class="shadow" width="500px">

### Verify

At this point, the migration is complete and your site should be live. For good measure, restart the app to make sure that
the site works across restarts.

