# Import MySQL 

## Overview

In this guide, we will see how to export a MySQL database from your current setup and import it into
the MySQL database of a Cloudron app.

## Export

The first step is to create a dump in your existing MySQL database setup:

```
mysqldump -hmyservername -umyusername -pmypassword --single-transaction --routines --triggers databasename > mysqldump.sql
```

If the MySQL server is on Cloudron, you can export it using this command line on the [Web Terminal](/apps#web-terminal):

```
mysqldump -h${CLOUDRON_MYSQL_HOST} -u${CLOUDRON_MYSQL_USERNAME} -p${CLOUDRON_MYSQL_PASSWORD} --single-transaction --routines --triggers ${CLOUDRON_MYSQL_USERNAME} > /tmp/mysqldump.sql
```

## Import

* After install, enable 'Recovery Mode'in the `Repair` section. This will ensure that the app is paused and not actively
  using the database while you are importing.

<center>
<img src="/guides/img/app-enable-recovery-mode.png" class="shadow" width="500px">
</center>

* Open a [Web Terminal](/apps#web-terminal) by using the Terminal button in the `Console` section.

<center>
<img src="/guides/img/app-terminal.png" class="shadow" width="500px">
</center>

* In the terminal, upload the dump file using the `Upload to /tmp` button.

<center>
<img src="/guides/img/app-upload.png" class="shadow" width="500px">
</center>

* In the terminal, clear the existing database:

```
root@838249e2-d2ae-4a40-80bf-4f1632e0d376:/app/code#  mysql --user=${CLOUDRON_MYSQL_USERNAME} --password=${CLOUDRON_MYSQL_PASSWORD} --host=${CLOUDRON_MYSQL_HOST} ${CLOUDRON_MYSQL_DATABASE} -Nse 'show tables' | while read table; do mysql --user=${CLOUDRON_MYSQL_USERNAME} --password=${CLOUDRON_MYSQL_PASSWORD} --host=${CLOUDRON_MYSQL_HOST} ${CLOUDRON_MYSQL_DATABASE} -e "SET FOREIGN_KEY_CHECKS = 0; drop table $table"; done
```

* In the terminal, import the dump using the `mysql` command. Click the `MySQL` button on top of the terminal to paste the
  command line required to access the MySQL database and redirect the dump file to the command:

```
root@838249e2-d2ae-4a40-80bf-4f1632e0d376:/app/code# mysql --user=${CLOUDRON_MYSQL_USERNAME} --password=${CLOUDRON_MYSQL_PASSWORD} --host=${CLOUDRON_MYSQL_HOST} ${CLOUDRON_MYSQL_DATABASE} < /tmp/mysqldump.sql
mysql: [Warning] Using a password on the command line interface can be insecure.
root@838249e2-d2ae-4a40-80bf-4f1632e0d376:/app/code#
```

An empty output like above means the command succeeded.

* To start the app again, click the `Disable Recovery Mode` in `Repair` section:

<center>
<img src="/guides/img/app-disable-recovery-mode.png" class="shadow" width="500px">
</center>


## Verify

Click the `MySQL` button on top of the terminal to paste the command line required to access the MySQL database.
You can now press enter/return to get the MySQL shell.

<center>
<img src="/guides/img/mysql-shell.png" class="shadow" width="500px">
</center>

