# Decrypt Backups

## Overview

Cloudron supports encryption of backups with a passphrase. In this guide, we will see
how to decrypt Cloudron backups on your laptop/PC.

## Prerequisites

Ensure your laptop/PC setup has the [Cloudron CLI tool](/custom-apps/cli/). You can do this
by `sudo npm install -g cloudron`.

Then, login using `cloudron login my.example.com`.

## Download backup

To download the backup locally, you need to know where the backup is stored and the backup ID.

There are two types of backups: App backups which are per app; and box backup which is for the Cloudron
platform itself.

App backup configuration can be downloaded from the app's Backup view:

<center>
<img src="/guides/img/app-backup-config.png" class="shadow" width="500px">
</center>

Box backup configuration can be downloaded from the `Backups` view:

<center>
<img src="/guides/img/cloudron-backup-config.png" class="shadow" width="500px">
</center>

The backup configuration contains a JSON blob that contains the backup ID and storage location of your backup.
You can download the backup itself using a variety of tools based on the storage provider.

For example, to download from S3, you can use the AWS CLI tools on Linux:

```
sudo apt install awscli

# for rsync backups

AWS_ACCESS_KEY_ID=access_key AWS_SECRET_ACCESS_KEY=secret_key aws --endpoint-url https://region.endpoint.com s3 sync s3://bucket/backupid .

# for tgz backups

AWS_ACCESS_KEY_ID=access_key AWS_SECRET_ACCESS_KEY=secret_key aws --endpoint-url https://region.endpoint.com s3 sync s3://bucket/backupid.tar.gz.enc .

```

## Decrypt

### tgz

If the backup format is tgz, the download will just be a single file.

```
cloudron backup decrypt --password=passphrase backupid.tar.gz.enc > backupid.tar.gz
tar zxvf backupid.tar.gz
```

### rsync

If the backup format is rsync, the download will just be a directory.

```
mkdir /path/to/unencrypted
cloudron backup decrypt-dir --password=passphrase backupid/ /path/to/unencrypted
```

