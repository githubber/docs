## Overview

In this guide, we will see how to determine the backup ID.

## Backup file names

Backups are categorized at the top level by timestamped directories. This timestamp
indicates the time at which the backup was started. In addition, there is a directory
called `snapshot`. The `snapshot` directory is a working directory for the backup logic
and should not be touched.

Inside each timestamped directory, Cloudron stores the backup of each app individually.
These backups have the file name, `app_<appid>_<timestamp>_v<package_version>`. When using
the `tgz` backup format, the file will have an extension `.tar.gz`. When using the `rsync`
backup format, this will be a directory.

Cloudron stores the backup of the platform itself with the file name `box_<timestamp>_v<box_version>`.

## Backup ID

When restoring the entire Cloudron, the backup ID is the file name of the box backup (without the extension).
So, it is `<timestamped_directory>/box_<timestamp>_v<box_version>`.

If you want to restore a specific app, the backup ID is `<timestamped_directory>/app_<appid>_<timestamp>_v<package_version>`.

Usually, you don't have to remember these backup IDs. You can download this information by clicking the
"Download Backup Configuration" button next to a backup. Be sure to save this in a secure and reliable location for later possible use.

<center>
<img src="/img/backups-backup-config.png" class="shadow" width="500px">
</center>

If you don't have access to Cloudron server, then you can determine the backup ID by examining the directories and files on the storage backend.

### File System

* Navigate to the backup directory and do a `ls -l`:

```
root@my:/var/backups# ls -l
drwxr-xr-x 2 yellowtent yellowtent   4096 Mar  9 23:16 2021-03-09-231654-893
drwxr-xr-x 2 yellowtent yellowtent   4096 Mar  9 23:36 2021-03-09-233635-050
drwxr-xr-x 2 yellowtent yellowtent   4096 Mar  9 23:39 2021-03-09-233900-284
drwxr-xr-x 2 yellowtent yellowtent   4096 Mar  9 23:40 2021-03-09-234040-603
drwxr-xr-x 2 yellowtent yellowtent   4096 Mar  9 23:42 2021-03-09-234229-615
drwxr-xr-x 2 yellowtent yellowtent   4096 Mar 10 07:00 2021-03-10-070000-732
```

* Pick the latest timestamped directory. In the example above, it is `2021-03-10-070000-732`. Now do `ls -l 2021-03-10-070000-732`.

```
root@my:/var/backups# ls -l 2021-03-10-070000-732
total 7380
-rw-r--r-- 2 yellowtent yellowtent 7132482 Mar 10 07:00 app_6626e3f6-deb4-438e-87ef-9464409c5d6b_2021-03-10-070002-342_v1.2.0.tar.gz
-rw-r--r-- 2 yellowtent yellowtent  420660 Mar 10 07:00 box_2021-03-10-070003-531_v6.2.2.tar.gz
```

The backup ID is `2021-03-10-070000-732/box_2021-03-10-070003-531_v6.2.2` (note that we dropped the extension). The backup version is `6.2.2`. You can now install Cloudron version 6.2.2 (`cloudron-setup --version 6.2.2`) and [restore Cloudron](/backups/#restore-cloudron).

### S3 (UI)

If you use one of the S3-compatible providers, you can usually go to the UI of the storage provider and find the latest timestamp.
For example, in DO spaces:

<center>
<img src="/img/backups-do-spaces-listing.png" class="shadow" width="500px">
</center>

In the screenshot above, we can see that `2021-03-10-182730-145` is the latest. When we click on that directory we see:

<center>
<img src="/img/backups-do-spaces-listing2.png" class="shadow" width="500px">
</center>

The backup ID is `2021-03-10-182730-145/box_2021-03-10-182734-919_v6.2.3` (note that we dropped the extension). The backup version is `6.2.3`. You can now install Cloudron version 6.2.3 (`cloudron-setup --version 6.2.3`) and [restore Cloudron](/backups/#restore-cloudron).

### S3 (AWS CLI)

In some cases, some S3 providers do not have a UI for the object storage, or the UI may simply fail to load if there are many objects.
In that case, we can use the S3 CLI to determine the backup ID.

* First, install the AWS CLI tool. On Linux, you can simply do `sudo apt install awscli`. For other platforms, see [this guide](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv1.html).

* List the buckets and objects using the CLI tool. Note that sometimes you have to set the `AWS_DEFAULT_REGION` to the region name of the provider, and sometimes to `us-east-1`. This setting depends on your provider. In the example below, `cloudron-backups2` is the name of the bucket. If you provided a `prefix`, then you must list `s3://cloudron-backups2/prefix` in the example below.

```
$ export AWS_ACCESS_KEY_ID=JWAJUADXZJU2LBPBQLG2
$ export AWS_SECRET_ACCESS_KEY=SomeSecretKeyThatCannotBePublic
$ export AWS_DEFAULT_REGION=us-east-1
$ aws --endpoint-url=https://sfo2.digitaloceanspaces.com s3 ls s3://cloudron-backups2/
                           PRE 2021-03-02-162423-447/
                           PRE 2021-03-02-162651-715/
                           PRE 2021-03-05-072427-396/
                           PRE 2021-03-10-182730-145/
                           PRE snapshot/
```

We can see above that the latest timestamp is `2021-03-10-182730-145`. Now we can list the contents of that directory.

```
$ aws --endpoint-url=https://sfo2.digitaloceanspaces.com s3 ls s3://cloudron-backups2/2021-03-10-182730-145/
2021-03-10 10:27:33    7129765 app_6626e3f6-deb4-438e-87ef-9464409c5d6b_2021-03-10-182732-640_v1.2.0.tar.gz
2021-03-10 10:27:34     422273 box_2021-03-10-182734-919_v6.2.3.tar.gz
```


The backup ID is `2021-03-10-182730-145/box_2021-03-10-182734-919_v6.2.3` (note that we dropped the extension). The backup version is `6.2.3`. You can now install Cloudron version 6.2.3 (`cloudron-setup --version 6.2.3`) and [restore Cloudron](/backups/#restore-cloudron).

