# Volumes

## Overview

Apps on Cloudron are containerized and do not have access to the server's file system. To provide an app access to a path on the server,
one can create a Volume and then [mount the volume](/app/#mounts) into the app.

## Usage

Volumes can be added in the `Volumes` view. Click the `Add Volume` button to add a volume and select the mount type.

<center>
<img src="/img/volumes-list.png" class="shadow" width="500px">
</center>


Once added, the volumes can be [mounted](/apps/#mounts) into one or more apps in the configuration page of the app.  When mounting into an app,
you can specify if it should be read-only or not.

<center>
<img src="/img/apps-mount.png" class="shadow" width="500px">
</center>

!!! note "Volumes are not backed up"
    Volumes are not backed up. Restoring an app will not restore the volume's content. Please make sure to have a suitable backup plan for each volume.

## Mount Type

Cloudron supports a variety of mount types. When using a mount type other than `No-op`, Cloudron will setup systemd mount config files
to automatically mount on server start up.

When using the `No-op` type, only paths under `/mnt`, `/media`, `/srv`, `/opt` can be added for security reasons. When using other providers,
a mount point is created under `/mnt/volumes`.

!!! warning "Do not create fstab entry"
    When using the NFS/EXT4/CIFS/SSHFS providers, do not add an entry in `/etc/fstab` because Cloudron will already set up the mount via systemd.
    Use the `No-op` provider if you want to add an `/etc/fstab` entry.

### CIFS

The CIFS mount type is used to mount CIFS shares. Note that, unlike EXT4 and NFS mount types, CIFS does not have a concept of
[users and groups](https://www.systutorials.com/docs/linux/man/8-mount.cifs/#lbAK). This makes it unsuitable for use as an
app's data directory but will work fine for volumes and backups.

<center>
<img src="/img/volumes-cifs.png" class="shadow" width="500px">
</center>

### EXT4

The EXT4 mount type is used to mount external hard disks or block storage. To add an external EXT4 disk, first make sure the
disk is formatted as EXT4 using `mkfs.ext4 /dev/<device>`. Then, run `blkid` or `lsblk` to get the UUID of the disk.

<center>
<img src="/img/volumes-ext4.png" class="shadow" width="500px">
</center>

### Filesystem

The Filesystem type is used for giving apps access to directories on the server. These are just directories on the local
filesystem and do not require any mounting configuration. We recommend giving this directory `chmod 777` permissions for
maximum compatibility across apps.

<center>
<img src="/img/volumes-filesystem.png" class="shadow" width="500px">
</center>

### Filesystem (mountpoint)

When using the `mountpoint` mount type, Cloudron will not configure the server to mount the mount point. You have to set up `/etc/fstab`
or systemd mount config files on your own. Use this if you want to set up an unsupported mount type or want to add specialized
mount flags.

<center>
<img src="/img/volumes-mountpoint.png" class="shadow" width="500px">
</center>

### NFS

The NFS mount type is used to mount NFS shares. If you need help setting up an NFS server, see [this article](https://www.tecmint.com/install-nfs-server-on-ubuntu).
By default, NFS shares will change the root user to be owned by the `nobody` user. This is done for security purposes since it
prevents creating files with setuid bit set. You can add `no_root_squash` to the options in the NFS server's exports file to
circumvent this.

<center>
<img src="/img/volumes-nfs.png" class="shadow" width="500px">
</center>

### SSHFS

The SSHFS mount type is used to mount a file system over SSH (using the SFTP protocol).

<center>
<img src="/img/volumes-sshfs.png" class="shadow" width="500px">
</center>

