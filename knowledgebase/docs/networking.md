# Networking

## IP Configuration

Using the IP configuration settings, you can configure the IP address, Cloudron uses
to configure to the DNS.

### Public IP

When using the `Public IP` provider, Cloudron will automatically detect the server's
public IP address by querying [this url](https://api.cloudron.io/api/v1/helper/public_ip).

<center>
<img src="/img/networking-public-ip.png" class="shadow" width="500px">
</center>

### Network Interface

If the server has multiple IP addresses, you can configure the preferred IP address by specifying
the network interface. The interfaces can be listed using `ip -f inet -br addr`.

<center>
<img src="/img/networking-interface-name.png" class="shadow" width="500px">
</center>

### Static IP

Use this option to provide a static IPv4 address. This IP address can be public or private.
Some use cases for using this provider are:

* [Digital Ocean Floating Address](https://www.digitalocean.com/docs/networking/floating-ips/)
* AWS VPC IP address
* [OVH Failover IP](https://docs.ovh.com/ca/en/public-cloud/configure_a_failover_ip/)

<center>
<img src="/img/networking-static-ip.png" class="shadow" width="500px">
</center>

## Private DNS

Cloudron uses the unbound DNS server internally to resolve all DNS queries
across all the apps.

If Cloudron is setup inside an internal network, you can customize the unbound
configuration to resolve internal hosts.

### Adding static hosts

To add some host entries internal to your network, add a file named `/etc/unbound/unbound.conf.d/custom.conf`:

```
server:

local-zone: "example.com." static
local-data: "jim.example.com. IN A 1.2.3.4"
local-data: "doug.example.com. IN A 2.3.4.5"
```

Be sure to restart the unbound service using `sudo systemctl restart unbound` and check it's status using
`sudo systemctl status unbound`.

### Internal DNS server

To add an internal DNS server, add a file named `/etc/unbound/unbound.conf.d/custom.conf`:

```
server:
    private-domain: "cloudron.lan"
    domain-insecure: "cloudron.lan"

forward-zone: 
    name: "cloudron.lan"
    forward-addr: 10.0.0.2
```

If your internal DNS server is not a forwarding server, use the `stub-zone:` and `stub-addr:` option instead.

Be sure to restart the unbound service using `sudo systemctl restart unbound` and check it's status using
`sudo systemctl status unbound`.

## Dynamic DNS

Enable this option to keep all your DNS records in sync with a changing IP address. This is useful when Cloudron
runs in a network with a frequently changing public IP address like a home connection.

<center>
<img src="/img/networking-dynamic-dns.png" class="shadow" width="500px">
</center>


## Firewall

### Blocklist

Using the blocklist configuration, one or more IP addresses and/or networks can be blocked from connecting to Cloudron.
You can download various country based blocklists from [here](https://www.ipdeny.com/ipblocks/).

<center>
<img src="/img/networking-blocklist.png" class="shadow" width="500px">
</center>

You can also add in comments to the line items as needed, but comments must remain on their own line, something similar to:

```
# spammy IP
111.111.111.111
```

!!! warning "Do not lock yourself out"
    Be careful about what IP addresses you block. If you lock yourself out, you must get Console access to the server,
    remove the file `/home/yellowtent/platformdata/firewall/blocklist.txt` and reboot the server.

### Whitelist ports

Cloudron does not support installing additional packages or running other services on the server. With that warning out
of the way, you can configure the firewall to permit additional (incoming) TCP and UDP ports. For this, edit the the file
`/home/yellowtent/platformdata/firewall/ports.json` (create this file if it does not exist and change the owner to the user
`yellowtent`).

```
{
    "allowed_tcp_ports": [ 2140, 3540 ],
    "allowed_udp_ports": [ ]
}
```

Restart the firewall to apply the configuration:

```
systemctl restart cloudron-firewall
```

