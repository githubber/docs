# User management

## Single Sign-On

Single Sign-on (SSO) is a feature of Cloudron where users can use the same credentials
(username & password) for logging in to apps.

SSO integration is optional and can be selected at the time of app installation. Turning off SSO
can be beneficial when the app is meant to be used primarily by external users (for example, a community
chat or a public forum).

Note that some apps do not support Cloudron SSO. Such apps manage their user credentials on their
own and users have to be managed inside the app itself.

When SSO is available for an app, the user management options will look like below:

<center>
<img src="/img/sso-available.png" class="shadow" width="500px">
</center>

When `Leave user management to the app` is selected, the app's Cloudron SSO integration will be disabled
and all user management has to be carried from within the app. This is useful when the app primarily caters
to external users (like say a community chat).

When SSO integration is unavailable for an app, the user management options look like below:

<center>
<img src="/img/sso-unavailable.png" class="shadow" width="500px">
</center>

## Users

New users can be added to the Cloudron with their email address from the `Users` menu.

<center>
<img src="/img/user-list.png" class="shadow" width="500px">
</center>

Click on `New User` to add a new user:

<center>
<img src="/img/users-add.png" class="shadow" width="500px">
</center>

They will receive an invite to sign up. Once signed up, they can access the apps they have been given access to.

To remove a user, simply remove them from the list. Note that the removed user cannot access any app anymore.

### Valid usernames

The following characters are allowed in usernames:

* Alphanumeric characters
* '.' (dot)
* '-' (hyphen)

Usernames must be chosen with care to accomodate the wide variety of apps that run on Cloudron.
For example, very generic words like `error`, `pull`, `404` might be reserved by apps.

## Groups

Groups provide a convenient way to group users. You can assign one or more groups to apps to restrict who can
access for an app.

You can create a group by using the `Groups` menu item.

<center>
<img src="/img/users-groups-list.png" class="shadow">
</center>

Click on `New Group` to add a new group:

<center>
<img src="/img/users-groups-add.png" class="shadow">
</center>


To set the access restriction use the app's configure dialog.

<center>
<img src="/img/app-configure-group-acl.png" class="shadow" width="500px">
</center>

### Valid group names

The following characters are allowed in group names:

* Alphanumeric characters
* '.' (dot)
* '-' (hyphen)

## Roles

Roles provide a way to restrict the permissions of a user. You can assign a role from the `Users` page.

<center>
<img src="/img/users-role.png" class="shadow" width="500px">
</center>

### User

A Cloudron user can login to the Cloudron dashboard and use the apps that they have access to.
They can edit their profile (name, password, avatar) on the dashboard.

### User Manager

A User Manager can add, edit and remove users & groups. Newly added users always get the `User` role.
User Manager cannot modify the role of an existing user.

### Administrator

A Cloudron administrator can manage apps and users. Note that an admin can login to any app even
if they have not been explicitly granted access in the `Access Control` section.

### Owner

A Cloudron owner has the capabilities of administrator with the addition of the ability to manage the
Cloudron subscription, manage backup settings and configure branding.

A good way to think about the owner role is a person who is in charge of server administration and billing.

!!! note "Automatic login"
    When clicking the `Manage Subscription` button in the `Settings` view, they are automatically logged
    in to the cloudron.io account.

## Password reset

### Users

The password reset mechanism relies on email delivery working reliably. Users can reset their own passwords.

In the event that [email delivery is not working](email/#debugging-mail-delivery), an administrator
can generate a new password reset link for another user by clicking on the 'Send invitation email' button.

<center>
<img src="/img/reinvite.png" class="shadow" width="500px">
</center>

This will open up a dialog showing the password reset link. If email delivery is not working for some
reason, the link can be sent to the user by some other means.

<center>
<img src="/img/invitation-dialog.png" class="shadow" width="500px">
</center>

### Admins

The password reset mechanism relies on email delivery working reliably. In the event that [email delivery is not working](email/#debugging-mail-delivery),
you can get a temporary password by SSHing into the server:

```
# sudo cloudron-support --owner-login
Login as superadminname / mW5x5do99TM2 . Remove /home/yellowtent/platformdata/cloudron_ghost.json when done.
```

The above command simply creates a temporary password for the superadmin user. There are some corner cases in which the above
command many not work. In that case, simply edit `/home/yellowtent/platformdata/cloudron_ghost.json` directly to have the
contents like:

```
{ "superadminname" : "somesecretpassword "}
```

You can now login as the superadmin with the username and password listed above.

* If you were intending to reset the password of the superadmin user, then go to the `Profile` and change the password.

* If you were intending to reset the password of another user, go to the User's view and click 'Reset password link' button.
  This will show a dialog which contains the reset URL. Simply, copy and paste the URL into your browser to reset that user's
  password.

Once done, be sure to remove the file `/home/yellowtent/platformdata/cloudron_ghost.json`. This will remove the
temporary password.

## Mandatory 2FA

Admins can require all users to set up two factor authentication by enabling the Mandatory 2FA setting.
To enable, use the setting in the `Users` view.

<center>
<img src="/img/users-mandatory-2fa.png" class="shadow" width="500px">
</center>

When enabled, all new users will be forced to setup a 2FA during sign up. Existing users will be forced to setup 2FA when they login
or reload the dashboard page.

<center>
<img src="/img/users-2fa-required.png" class="shadow" width="500px">
</center>

## Disable 2FA

If a user loses their 2FA device, the Cloudron administrator can disable the user's
2FA setup using the password reset button.

<center>
<img src="/img/users-2fa-reset.png" class="shadow" width="500px">
</center>

Once disabled, user can login with just their password. After login, they can
re-setup 2FA.

## Disable user

To disable a user, uncheck the `User is active` option. Doing so, will invalidate all
existing Cloudron dashboard session of the user and will log them out. The user may
still be able to use any apps that they were logged into prior to the de-activation.
To log them out from the apps, you can check if the app provides a way to log them out
(support for this depends on the app).

<center>
<img src="/img/user-disable.png" class="shadow" width="500px">
</center>

!!! note "Disabling does not delete user data"
    Disable a user only blocks the login access for the user. Any data generated by the
    user inside apps is not deleted.

## Lock profile

Admins can disallow users from changing their email and full name by locking user profiles. To
lock the profile, simple uncheck the setting in the `Users` view.

<center>
<img src="/img/users-lock-profile.png" class="shadow" width="500px">
</center>

## Impersonate user

One can create a file named `/home/yellowtent/platformdata/cloudron_ghost.json` which contains an username
and a fake password like:

```
{"girish":"secret123"}
```

With such a file in place, you can login to the Webadmin UI using the above credentials
(the user has to already exist). This helps you debug and also look into how the UI might
look from that user's point of view.

## External LDAP

The LDAP connector allows users from your existing LDAP or active directory to authenticate with Cloudron.
Each user account from the external directory will be automatically created on Cloudron and kept up-to-date.

Optionally groups can also be synced. Externally defined groups will be automatically created and users will be associated.
Currently groups removed from the external source will not be deleted on the Cloudron during a sync.

For the moment, user synchronisation has to be **manually** triggered from within the dashboard using the
**Synchronize** button. A future release will synchronize users periodically.

The External LDAP sever configuration is set in the `Users` view.

<center>
<img src="/img/external-ldap-overview.png" class="shadow" width="100%">
</center>

### JumpCloud

The following screenshot shows the available configure options using a jumpcloud external LDAP directory:

* `Server URL`: `ldaps://ldap.jumpcloud.com:636`
* `Base DN`: `ou=users, o=3214565, dc=jumpcloud, dc=com`
* `Filter`: `(objectClass=inetorgperson)`
* `Bind DN`: `uid=ldap_admin,ou=Users,o=3214565,dc=jumpcloud,dc=com`
* `Bind password`: `admin password`
* `Username field`: `uid`

### Okta

To use the Okta integration, do the following:

* In Okta, enable the [LDAP interface](https://help.okta.com/en/prod/Content/Topics/Directory/LDAP_Using_the_LDAP_Interface.htm).
  You can do this from the `Directory Integrations` page.

* By default, Okta uses email as the default uid. Cloudron requires usernames for LDAP integration to work. If you already have a
  field in Okta that can provide usernames, provide that as the `username field`. If not, you can create a new field in the
  profile editor and set that.

* Cloudron configuration (replace 'org' below):
    * `Server URL`: `ldaps://<org>.ldap.okta.com`
    * `Base DN`: `ou=users, dc=<org>, dc=okta, dc=com`
    * `Filter`: `(objectClass=inetorgperson)`
    * `Bind DN`: `uid=<admin>, dc=<org>, dc=okta, dc=com`
    * `Bind password`: `admin password`
    * `Username field`: see above

<center>
<img src="/img/external-ldap-configuration.png" class="shadow" width="500px">
</center>


