# Storage

## App Data Directory

Apps store their data and assets in the `/home/yellowtent/appsdata/<appid>` directory. If the
server is running out of disk space (in the root filesystem), you can move the app's storage
directory to another location. In most cases, this is an external disk mounted on the server. For example,
you can mount a DigitalOcean [Block Storage](https://www.digitalocean.com/docs/volumes/) or AWS [Block Store](https://aws.amazon.com/ebs/)
and move the app's data to that disk.

For example, to move an app's data to an external disk location like `/mnt/nextcloud`, go to the `Storage` section:

<center>
<img src="/img/app-custom-data.png" class="shadow" width="500px">
</center>

!!! note "App Data Directory is backed up"
    The external app data directory is part of the app's backup.

## Disk layout

Most of the Cloudron code and data is sandboxed in `/home/yellowtent` (For the curious,
`yellowtent` was the code name for Cloudron). The subdirectories are:

* `box` - This contains the Cloudron code. The Cloudron code does not run as root.
* `boxdata` - This contains data that is generated by the Cloudron code including certs. This also contains all user emails in the `mail/vmail` directory in the maildir format.
* `appsdata` - This contains the data generated by each app. Each directory here corresponds
  to the application id.
* `platformdata` - This contains 'runtime' data of the platform for mysql, postgres,
  mongodb, nginx.

`boxdata`, `appsdata` and `platformdata` are [relocatable](#default-data-directory).

The other important locations on the server are:

* `/var/lib/docker` - This contains docker images. This is [relocatable](#docker-images).
* `/etc/nginx` - These contains the reverse proxy configuration. It is not meant to be edited manually.
* `/apps.swap` - This is a swap file that is dynamically resized by Cloudron. It will be no more than 4GB. You
  can resize this to any size after the initial setup. If you want to disable the swap altogether, just truncate the
  file to 0 size.

## Docker images

Cloudron uses Docker for containerizing applications and docker images tend to consume a lot of space.
The docker images are stored by default at `/var/lib/docker`. They can be moved to an external storage
as follows:

* Stop docker and the box code:

```
systemctl stop docker box
```

* Change docker image location to say `/mnt/docker` by creating a systemd drop-in `/etc/systemd/system/docker.service.d/custom.conf` with
the following contents:

```
[Service]
ExecStart=
ExecStart=/usr/bin/dockerd -H fd:// --log-driver=journald --exec-opt native.cgroupdriver=cgroupfs --storage-driver=overlay2 --graph=/mnt/docker
```

* Start docker

```
systemctl daemon-reload
systemctl start docker
systemctl status docker
docker network create --subnet=172.18.0.0/16 --ip-range=172.18.0.0/20 cloudron
```

* Now edit the file `/home/yellowtent/platformdata/INFRA_VERSION`. Bump the `version` field carefully by only adjusting the last digit from `48.18.0` to `48.18.1`. Do not change the other digits. This is just a way to inform the cloudron code that the infrastructure has changed and it has to re-create containers.

* Reboot the server. This is required for the `docker network` to start working in many VPSes (for unknown reasons).

* On reboot, addons and app images will be recreated preserving existing data. This can take a while. You can use `tail -f /home/yellowtent/platformdata/logs/box.log` to see the progress.

* Finally, after everything is up, you can remove the old docker images

```
rm -rf /var/lib/docker
```

!!! note "Docker image size"
    The `du -hcs /var/lib/docker` command will often return incorrect output with the overlay2 driver
    because of [how du works](https://stackoverflow.com/questions/50589971/diffrence-between-real-and-showed-by-docker-system-df-disc-usage).
    Use `docker system df` instead to determine the size

!!! note "Docker storage driver"
    Cloudron apps are only tested using the overlay2 graph driver. Other storage drivers are not tested. In the past, we have seen
    some apps to not work correctly with storage drivers like devicemapper.

## Default Data Directory

Please make sure you have a [complete backup](backups/#making-a-complete-backup) before
following the procedure below.

Apps store their data under `/home/yellowtent/appsdata`. Cloudron itself stores it's data (users, groups, certs, mails etc)
under `/home/yellowtent/boxdata`.

If the server is running out of disk space, one or more of these directories can be moved to another ext4 disk/location as follows:

```
    systemctl stop box
    systemctl stop docker
    DATA_DIR="/var/data"  # this is the external disk location
    mkdir -p "${DATA_DIR}"

    # (optional) move out apps data to the external disk
    mv /home/yellowtent/appsdata "${DATA_DIR}"
    ln -s "${DATA_DIR}/appsdata" /home/yellowtent/appsdata

    # (optional) move out box data to the external disk
    mv /home/yellowtent/boxdata "${DATA_DIR}"
    ln -s "${DATA_DIR}/boxdata" /home/yellowtent/boxdata

    # (optional) move out app database storage to the external disk
    mv /home/yellowtent/platformdata "${DATA_DIR}"
    ln -s "${DATA_DIR}/platformdata" /home/yellowtent/platformdata

    systemctl start docker
    systemctl start box
```

If the disk graph does not display properly, do a `systemctl restart collectd`.

**Note**: data directory must be an `ext4` filesystem.

## Server Resize

For VPS providers that support it, the server (cpu/disk/memory) can be resized and
Cloudron will automatically adapt to the available resources after a server restart.

!!! note "IP changes"
    Some VPS providers change the IP address of the virtual machine after a resize.
    You will have to change the IP address in your DNS, if this is the case.

### AWS EBS

On AWS, the partition and the filesystem must be resized after resizing the EBS
volume. This can be done by following the [AWS Guide](http://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-expand-volume.html).

## Swap

To give the server more swap space, run the following commands:

```
dd if=/dev/zero of=/extra.swap bs=1024 count=2097152
chmod 600 /extra.swap
mkswap /extra.swap
swapon /extra.swap
```

To make sure the swap persists reboots, add a line in `/etc/fstab`:

```
/extra.swap  none  swap  sw  0 0
```
