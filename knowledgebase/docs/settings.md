# Settings

## Timezone

The Cloudron server is configured to be in UTC. This is intentional and should not be changed.

Cloudron has an internal timezone setting that controls various cron jobs like backup, updates,
date display in emails etc. This timezone can be changed from the settings view.

<center>
<img src="/img/settings-timezone.png" class="shadow" width="500px">
</center>

## Language

The default language of Cloudron is English. This can be changed using the Language settings.
When set, users will be sent out invitation and reset emails using the selected language.
Note that users can always set a different language in their [profile](/profile/#language).

<center>
<img src="/img/settings-language.png" class="shadow" width="500px">
</center>


## Private Docker Registry

A private docker registry can be setup to pull the docker images of [custom apps](/custom-apps/tutorial/).

<center>
<img src="/img/settings-private-registry.png" class="shadow" width="500px">
</center>

!!! note "Use docker.io and not docker.com"
    If you are using the private DockerHub registry, use `docker.io` and not `docker.com`

