# Email

## Overview

Cloudron has a built-in mail server that can send and receive email on behalf of users
and applications. By default, most of it's functionality is disabled and it only sends out
mails on behalf of apps (for example, password reset and notification emails).

When `Cloudron Email` is [enabled](#enabling-email), it becomes a full-fleged mail server solution.
Each user gets a mailbox `username@domain` and can send mails using SMTP and receive mails
using IMAP. Users can also setup server side mail filtering rules using ManageSieve.

Features of this mail solution include:

* Multi-domain support
* Enable mailboxes for users and groups on a domain level
* Per-user and group mail aliases
* Mailbox sharing amongst users
* Group email addresses that forward email to it's members
* Email account sub-addressing by adding `+` tag qualifier
* Setup mail filters and vacation email using ManageSieve
* Catch all mailbox to receive mail sent to a non-existent mailbox
* Relay all outbound mails via SendGrid, Postmark, Mailgun, AWS SES or a Smart host
* Anti-spam. Users can train the spam filter by marking mails as spam. Built-in rDNS and
  zen spamhaus lookup. Admins can add custom spam rules for the entire server.
* Webmail. The [Rainloop](https://cloudron.io/appstore.html?app=net.rainloop.cloudronapp) and
  [Roundcube](https://cloudron.io/appstore.html?app=net.roundcube.cloudronapp) apps are already
  pre-configured to use Cloudron Email
* Completely automated DNS setup. MX, SPF, DKIM, DMARC are setup automatically
* Let's Encrypt integration for mail endpoints
* Domains and IP addresses blacklisting
* Server-side mail signatures (can be set per domain)
* [REST API](/api/) to add users and groups
* [Secure](/security/#email) out of the box
* Full text search using Solr
* Event Log

<br/>

Email settings are located under the `Email` menu item.

<center>
<img src="/img/mail-settings.png" class="shadow" width="200px">
</center>

## Setup

### Mail Server Location

By default, the location of the email server defaults to the Cloudron dashboard location i.e `my.domain.com`.
This can be changed in the `Mail` settings view. Cloudron will automatically setup the required DNS records
for all the domains when you change the mail server location. Any installed webmail clients will be automatically
re-configured as well. Be sure to adjust the settings of any mobile and desktop mail clients accordingly.

<center>
<img src="/img/email-change-location.png" class="shadow" width="500px">
</center>

### Send Test Email

To send a test email, click the `Send Test Email` button.

<center>
<img src="/img/email-send-test-email-button.png" class="shadow" width="500px">
</center>

A dialog will popup where you can enter the email address to send the test email to:

<center>
<img src="/img/email-send-test-email.png" class="shadow" width="500px">
</center>

### Enable Email

By default, Cloudron's mail server only sends email on behalf of apps. To enable users to
**receive** email, turn on the option under `Settings`.

<center>
<img src="/img/mail-enable.png" class="shadow" width="500px">
</center>

When the `Setup Mail DNS records now` option is checked, Cloudron will automatically update
the `MX`, `SPF`, `DKIM`, `DMARC` DNS records of the domain.

### Required ports

The following TCP ports must be opened in the firewall for Cloudron to send email:

* Outbound Port 25

If outbound port 25 is blocked by your server provider, [setup an email relay](#relaying-outbound-mails).
You can check if outbound port 25 is blocked by sending yourself a [test email](#send-test-email)
from the Cloudron.

The following TCP ports must be opened in the firewall for Cloudron to receive email:

* Inbound and Outbound Port 25
* Inbound Port 587 (SMTP/STARTTLS)
* Inbound Port 993 (IMAPS)
* Inbound Port 4190 (ManageSieve for email filters)

### Mail server status

Make sure that all the mail checks are green in the Email UI. Please note that the UI below
displays the check list depending on whether incoming email is enabled and whether a mail
relay is setup.

<center>
<img src="/img/mail-checks.png" class="shadow" width="500px">
</center>

If one or more checkboxes are not green, see the [debugging section](#debugging-mail).

## Mailbox

### Add

Mailboxes can be created for Users and Groups on a per-domain level. To do so, simply create
them in the `Email` view.

<center>
<img src="/img/mail-add-mailbox.png" class="shadow" width="500px">
</center>

The `Mailbox Owner` dropdown can be used to select an existing user or group. The user can then access their
email using the new email and the Cloudron password using [SMTP](#smtp-settings-for-cloudron-email) and
[IMAP](##imap-settings-for-cloudron-email).

When a group is selected as the owner, any member of the group can access the mailbox with their password.

Mailboxes have the following naming restrictions:

* Only alphanumerals, dot and '-' are allowed
* Maximum length of 200 characters
* Names ending with `.app` are reserved by the platform for applications
* Names with `+` are not allowed since this conflicts with the [Subaddresses and tags](##subaddresses-and-tags)
  feature.

### Remove

Use the delete button to delete a mailbox.

<center>
<img src="/img/email-remove-mailbox.png" class="shadow" width="600px">
</center>

After deletion, emails to this mailbox will bounce. If you have a catch-all address set, then emails
will get delivered to that mailbox.

!!! note "Deleting old emails"
    Deleting the mailbox does not remove old emails. You can remove the emails by removing the directory
    `/home/yellowtent/boxdata/mail/vmail/<mailbox@domain.com>`.

### Disable

A mailbox can be temporarily disabled by unchecking the `Mailbox is active` check box.

<center>
<img src="/img/mail-disable-mailbox.png" class="shadow" width="500px">
</center>

Once disabled, emails to this mailbox will bounce. If you have a catch-all address set, then emails
will get delivered to that mailbox.

## Mail aliases

One or more aliases can be configured for each mailbox. Aliases can be on the same domain or another domain.
You can do this by editing the mailbox configuration:

<center>
<img src="/img/mail-alias.png" class="shadow" width="500px">
</center>

!!! note "Authenticating with alias is not supported"
    Currently, it is not possible to login using the alias for SMTP/IMAP/Sieve services. Instead,
    add the alias as an identity in your mail client but login using the Cloudron credentials.

## Mailing Group

A Mailing group forwards emails to one or more email addresses. A list can be created in the `Email` view.

<center>
<img src="/img/mail-add-maillist.png" class="shadow" width="500px">
</center>

Use the 'Restrict posting to members only' option to allow only members to post to the list. When enabled,
non-members will get a bounce.

To support forwarding mails to external address, Cloudron implements [SRS](http://www.open-spf.org/SRS).
SRS translation is performed using the mailing list domain before forwarding the mail.

!!! note "No subscribe/unsubscribe feature"
    Cloudron does not support creating a mailing list (i.e) a list that allows members to
    subscribe/unsubscribe.

## Catch-all address

A Catch-all or wildcard mailbox is one that will "catch all" of the emails addressed
to non-existent addresses. You can forward such emails to one or more user mailboxes
in the Email section. Note that if you do not select any mailbox (the default), Cloudron
will send a bounce.

<center>
<img src="/img/catch-all-mailbox.png" width="500" class="shadow">
</center>



## Email Client Configuration

### IMAP

Use the following settings to receive email via IMAP:

  * Server Name - Use the mail server location of your Cloudron (default: `my` subdomain of the primary domain)
  * Port - 993
  * Connection Security - TLS
  * Username/password - Use the email id as the username and the Cloudron account password

!!! note "Multi-domain setup credentials"
    Use the email id as the username to access different mailboxes. For example, if email is
    enabled on two domains `example1.com` and `example2.com`, then use `user@example1.com`
    to access the `example1.com` mailbox and use `user@example2.com` to access the `example2.com`
    mailbox. In both cases, use the Cloudron account password.

### SMTP

Use the following settings to send email via SMTP:

  * Server Name - Use the mail server location of your Cloudron (default: `my` subdomain of the primary domain)
  * Port - 587
  * Connection Security - STARTTLS
  * Username/password - Use the full email as the username and the Cloudron account password

!!! note "Multi-domain setup credentials"
    Use the email id as the username to send email. For example, if email is
    enabled on two domains `example1.com` and `example2.com`, then use `user@example1.com`
    to send email as `example1.com` and use `user@example2.com` to send email as `example2.com`.
    In both cases, use the Cloudron account password.

### Sieve

Use the following settings to setup email filtering users via ManageSieve.

  * Server Name - Use the mail server location of your Cloudron (default: `my` subdomain of the primary domain)
  * Port - 4190
  * Connection Security - STARTTLS
  * Username/password - Use the full email as the username and the Cloudron account password

!!! note "Multi-domain setup credentials"
    Use the email id as the username to access different mailboxes. For example, if email is
    enabled on two domains `example1.com` and `example2.com`, then use `user@example1.com`
    to access the `example1.com` mailbox and use `user@example2.com` to access the `example2.com`
    mailbox. In both cases, use the Cloudron account password.

## Subaddresses and tags

Emails addressed to `<username>+tag@<domain>` i.e mail addresses with a plus symbol in the
username will be delivered to the `username` mailbox. You can use this feature to give out emails of the form
`username+kayak@<domain>`, `username+aws@<domain>` and so on and have them all delivered to your mailbox.

This trick works for email aliases as well.

## Relay outbound mails

By default, Cloudron's built-in mail server sends out email directly to recipients.
You can instead configure the Cloudron to hand all outgoing emails to a 'mail relay'
or a 'smart host' and have the relay deliver it to recipients. Such a setup is useful when the Cloudron
server does not have a good IP reputation for mail delivery or if server service provider
does not allow sending email via port 25 (which is the case with Google Cloud and Amazon EC2).

Cloudron can be configured to send outbound email via:

* [Amazon SES](#amazon-ses)
* Elastic Email
* Google
* Mailgun
* Mailjet
* Postmark
* Sendgrid
* [Office 365](#office-365)
* Sparkpost
* [External SMTP server](#smtp-server)

To setup a relay, enter the relay credentials in the Email section. Cloudron only supports relaying
via the STARTTLS mechanism (usually port 587).

<center>
<img src="/img/email-relay.png" width=500 class="shadow">
</center>

### Amazon SES

To setup Cloudron to relay via Amazon SES:

* Go to Amazon SES dashboard and add a new domain to verify under `Domains`. Leave the `Generate DKIM Settings` unchecked
  since Cloudron has already generated DKIM keys.
    * Setup the DNS records as suggested by the `Verify a New Domain`. SES will automatically set
      these up if your domain is on AWS Route53.

* Once domain is verified, click on `SMTP Settings` on the left pane and then click the `Create My SMTP Credentials`
  button.
    * Follow through the wizard to create a new IAM user that has the following policy

    ```
        "Statement": [{  "Effect":"Allow",  "Action":"ses:SendRawEmail",  "Resource":"*"}]
    ```

* Setup the relay on the Cloudron under the Email section:

<center>
<img src="/img/email-relay-ses.png" width=500 class="shadow">
</center>

* Use the [Send Test Email](#send-test-email) button to verify emails are sent.

* If you do not receive the email, please verify that your AWS SES is not in sandbox mode. In this mode, new AWS
   accounts are only able to send mails to verified domains or the simulator. You can check this in the
   `Sending Statistics` page and looking for a note that looks like below:

<center>
<img src="/img/ses-sandbox.png" width=500 class="shadow">
</center>

To remove sandbox, log a request to increase the sending limit to say 500 emails a day. Note that,
a [custom MAIL FROM domain](https://docs.aws.amazon.com/ses/latest/DeveloperGuide/mail-from.html) must
be set for the DMARC alignment to succeed.

### Google

When using Google to relay mail, if you encounter an error message of the form `Invalid login` or
`Please log in via your web browser and then try again`, you must configure your Google account
to either use App passwords or enable less secure apps. See [Google Support](https://support.google.com/mail/answer/7126229?visit_id=1-636433350211034673-1786624518&rd=1#cantsignin) for more information.

### Office 365

To setup Office 365 as relay, add a connector under mail flow following the instructions under [Option 3](https://support.office.com/en-us/article/How-to-set-up-a-multifunction-device-or-application-to-send-email-using-Office-365-69f58e99-c550-4274-ad18-c805d654b4c4). Note that relaying via Office 365 requires port 25 to be open and requires a static IP.

### SMTP Server

Cloudron can relay via an external SMTP server with or without authentication. Use `External SMTP server`
option for relaying via a server with a username/password. For IP based authentication relays, use the
`External SMTP sever (No authentication)`.

## Forward all emails

To forward some or all emails to an external address, create a Sieve filter. Sieve
filters can be created using [Rainloop](/apps/rainloop/),
[Roundcube](/apps/roundcube/) or any other client that supports
Manage Sieve.

<center>
<img src="/img/forward-all-emails-rainloop.png" class="shadow" width="600px">
</center>

To support forwarding mails to external address, Cloudron implements [SRS](http://www.open-spf.org/SRS).

## Marking Spam

The spam detection agent on the Cloudron requires training to identify spam.
To do this, simply move your junk mails to the pre-created folder named `Spam`.
Most mail clients have a Junk or Spam button which does this automatically.

If you marked a mail as Spam incorrectly, just move it out to the Inbox and
the server will unlearn accordingly.

The mail server is configured to act upon training only after seeing atleast
50 spam and 50 ham messages.

## Blacklist addresses and domains

Use the spam filter configuration UI in the `Email` view to blacklist addresses
and domains. Matched addresses will end up in the user's Spam folder. `*` and `?`
glob patterns are supported. This is a global setting and applies for incoming
mail to all domains.

<center>
<img src="/img/mail-spam-blacklist.png" class="shadow" width="500px">
</center>

When an email matches the above addresses, it will have `USER_IN_BLACKLIST` field
set in the `X-Spam-Status` email header.

!!! note "Blocking IP Addresses"
    To block an IP address or an entire network, use the [Firewall](network/#firewall)
    configuration.

## Custom Spam Filtering Rules

Custom spam filter rules can be set in the spam filter configuration UI in the `Email`
view. Spam filtering rules are global and applies for incomgng mail to all domains.

<center>
<img src="/img/mail-spam-rules.png" class="shadow" width="500px">
</center>

A simple Spam Assassin configuration rule to mark all emails containing the word 'discount'
in the subject looks like this:

```
header SUBJECT_HAS_DISCOUNT  Subject =~ /\bdiscount\b/i
score SUBJECT_HAS_DISCOUNT   100
describe SUBJECT_HAS_DISCOUNT    I hate email discounts
```

See this [guide](https://cwiki.apache.org/confluence/display/SPAMASSASSIN/WritingRules)
for writing custom rules.

## Change FROM address of an app

By default, Cloudron allocates the `location.app@domain` mailbox for each installed app. When
an app sends an email, the FROM address is set to `location.app@domain.com`. The mailbox name
can be changed in the [configure dialog](apps/#configuring-an-app) of the app.

<center>
<img src="/img/apps-mailbox-name.png" class="shadow" width="600px">
</center>

## Disable FROM address validation

By default, the Cloudron does not allow masquerading - one user cannot send email pretending
to be another user. To disable this, enable masquerading in the Email settings.

<center>
<img src="/img/email-masquerading.png" class="shadow" width="600px">
</center>

## Max mail size

The maximum size of emails that can be sent can be set using the Maximum Mail Size setting.

<center>
<img src="/img/email-max-size.png" class="shadow" width="600px">
</center>

Note that each mail client has it's own attachment size limits. See
[Rainloop docs](/apps/rainloop/#attachment-size) for more information.

## Vacation mail

An out of office / vacation mail message can be setup using Sieve filters. When using
Rainloop, a vacation message can be set in `Settings` -> `Filters` -> `Add filter` -> `Vacation message` action.

<center>
<img src="/img/email-vacation-message-rainloop.png" class="shadow" width="600px">
</center>

## Signature

A disclaimer, confidentiality information or legalese can be appended to every outbound email via the  Email Signature setting.
This setting is per-domain.

<center>
<img src="/img/email-signature.png" class="shadow" width="600px">
</center>

## Event log

Mail server activity can now be monitored using the Eventlog UI.

<center>
<img src="/img/email-eventlog.png" class="shadow" width="600px">
</center>

## Full Text Search

By default, every text search involves scanning mails over and over. With a small number of email (< 5GB), the performance
of search is usually acceptable. If there are a large number of emails, the emails can be indexed to make make search faster.

To enable the search index, enable Full Text Search (Solr) from the Email settings:

<center>
<img src="/img/email-fts-enable.png" class="shadow" width="600px">
</center>

Note that because the indexer consumes a lot of memory, Cloudron might decide to not run it if the mail server has
not been allocated enough memory. The status of the indexer is seen in the Email view:

<center>
<img src="/img/email-fts-status.png" class="shadow" width="600px">
</center>

Emails are automatically indexed as they come in and no manual intervention is required. If you enable indexing on an
server with existing mails, the first search triggers indexing of that mailbox. If that mailbox has a lot of mail,
then the first search can take a long time (maybe even up to 10 mins for 1GB of mail, it all depends on how fast your
server is).

Both, [Roundcube](/apps/roundcube/#search) and [SOGo](/apps/sogo/#search) can take advantage of indexed search.

!!! note "High Resource use"
    Solr (the indexer) consumes a lot of memory and disk space. To use it, you must allocate atleast 3GB for the mail service.

## Mailbox Sharing

Users can share mailboxes with each other (using IMAP ACLs). SOGo and Roundcube are pre-configured to use this feature.

For example, a user can share a folder of their mail account with another user (this screenshot is from Roundcube):

<center>
<img src="/img/email-sharing.png" class="shadow" width="600px">
</center>

The other user, can see the shared folder in their account:

<center>
<img src="/img/email-shared.png" class="shadow" width="600px">
</center>

!!! note "Manual migration"
    Mailbox sharing does not work out of the box for Cloudrons that were installed before 6.0.
    Follow [this guide](/guides/mailbox-sharing) to migrate.

## Alternate MX

A failover/alternate/backup MX can be setup for a domain on Cloudron. To set this up, do the following:

* Enable incoming mail for the Cloudron domain

* Cloudron has a anti-spoof feature to ensure that only it can generate emails for the incoming domains. This
feature will prevent the external MX from forwarding emails to it. However, Cloudron skips this spoof check
for servers listed in the domain's SPF record. So, white list the MX's IP address block in the domain's SPF record.
Note that it is necessary to specifically whitelist the server(s). Just a permissive SPF with `~all` and `?all` is not
enough.

## Import email

Existing mail can be imported into Cloudron via IMAP using a tool like [imapsync](https://imapsync.lamiral.info/dist/).

The steps to import email are:

* Add the domain for which you would like to receive email. To give the import a test run,
  uncheck the `Setup Mail DNS records now` option when enabling email for the domain. This
  will allow you to continue to use the email with your current provider.

  <center>
  <img src="/img/mail-enable-no-dns-setup.png" class="shadow" width="500px">
  </center>

* Create the mailbox(es) to import in Cloudron

* Use a tool like [IMAP Sync](https://imapsync.lamiral.info/) to import Email into Cloudron. For example,
  to [import from GMail](https://github.com/imapsync/imapsync/blob/master/FAQ.d/FAQ.Gmail.txt), use the
  following command:

```
    ./imapsync --gmail1 --user1 girish@example.com --password1 MASKED --host2 my.example.com --user2 girish@example.com --password2 MASKED --maxbytespersecond 20000 --useheader=X-Gmail-Received --useheader Message-Id --automap --regextrans2 s,\[Gmail\].,, --skipcrossduplicates   --folderlast [Gmail]/All Mail --exclude "\[Gmail\]/Spam"
```

* Once imported, you can verify if the mails look correct using apps like Rainloop or Roundcube.

* Once all mailboxes are imported, Cloudron has to be set as the mail server in the DNS. This can be done
  by clicking the `Re-setup DNS` button in the `Email` -> `Status` page.

  <center>
  <img src="/img/mail-resetup-dns.png" class="shadow" width="500px">
  </center>

## Archived Emails

When deleteing a mailbox, you can choose whether to delete the content (emails) inside the mailbox.
If you choose to keep the emails, then you can find the archived emails at `/home/yellowtent/boxdata/mail/vmail/`.

## Autodiscover

Auto discover is a feature where an email client is able to automatically detect the SMTP and IMAP configuration
from the email address. Setting up auto discover is very client specific.

### Autoconfig.xml

The [autoconfig.xml config format](https://wiki.mozilla.org/Thunderbird:Autoconfiguration:ConfigFileFormat) is supported by
many email clients like Thunderbird, K-9, KMail, Evolution, Kontact & others. Cloudron automatically serves up the autoconfig.xml
for all configured mail domains. Note that this feature requires an app to be installed on each of the email bare domains
i.e `domain1.com`, `domain2.com`, `domain3.com` and so on. These bare domains can also be just [redirects](/apps/#redirections)
to an existing app. If the bare domain is hosted outside Cloudron, see this [forum post](https://forum.cloudron.io/topic/4742/autoconfig-for-mail-when-website-not-hosted-on-cloudron)
for instructions.

!!! note "Requires app on bare domain"

    If you already had an app installed on the bare domain before Cloudron 6.1, you must go to the app's location section and click
    on 'Save' (without making any changes). This makes Cloudron re-generate the nginx configuration for the app which is required 
    for this feature to work.

### Outlook

!!! warning "Doesn't work"
    This method does not seem to work with Microsoft Outlook 2016, Microsoft Outlook 2019 or Microsoft Outlook for Office 365.
    Your mileage may vary.

Outlook performs an HTTP POST request to the following in order:

* `https://example.com/autodiscover/autodiscover.xml`
* `https://autodiscover.example.com/autodiscover/autodiscover.xml`
* DNS SRV lookup of `_autodiscover._tcp.example.com` and use the response to look up `https://srvresponse/autodiscover/autodiscover.xml.
  The SRV record is of the format `0 0 443 ssl.mailprovider.com`.

Because, it is a POST request, you cannot use some static hosting setup. For example, you can use a PHP file like below (there are also
more [sophisticated](https://github.com/gronke/email-autodiscover) attempts):

```
<?php
$raw = file_get_contents('php://input');
$matches = array();
preg_match('/<EMailAddress>(.*)<\/EMailAddress>/', $raw, $matches);
header('Content-Type: application/xml');
?>
<Autodiscover xmlns="http://schemas.microsoft.com/exchange/autodiscover/responseschema/2006">
  <Response xmlns="http://schemas.microsoft.com/exchange/autodiscover/outlook/responseschema/2006a">
    <User>
      <DisplayName>Cloudron</DisplayName>
      <EMailAddress><?php echo $matches[1]; ?></EMailAddress>
    </User>
    <Account>
      <AccountType>email</AccountType>
      <Action>settings</Action>
      <Protocol>
        <Type>IMAP</Type>
        <Server>my.example.com</Server>
        <Port>993</Port>
        <DomainRequired>off</DomainRequired>
        <SPA>off</SPA>
        <SSL>on</SSL>
        <AuthRequired>on</AuthRequired>
        <LoginName><?php echo $matches[1]; ?></LoginName>
      </Protocol>
      <Protocol>
        <Type>SMTP</Type>
        <Server>my.example.com</Server>
        <Port>587</Port>
        <DomainRequired>off</DomainRequired>
        <SPA>off</SPA>
        <Encryption>TLS</Encryption>
        <AuthRequired>on</AuthRequired>
        <LoginName><?php echo $matches[1]; ?></LoginName>
      </Protocol>
    </Account>
  </Response>
</Autodiscover>
```

## SRS

[Sender Rewriting Scheme](https://en.wikipedia.org/wiki/Sender_Rewriting_Scheme) is a scheme for
rewriting the envelope sender address of an email message. This method was devised to forward email
without breaking SPF.

Cloudron mail server implements SRS for forwarded emails. Cloudron's SRS implementation is in-line
with email providers like [Office 365](https://docs.microsoft.com/en-us/office365/troubleshoot/antispam/sender-rewriting-scheme)
and [namecheap](https://www.namecheap.com/support/knowledgebase/article.aspx/10204/2214/email-forwarding-why-its-important-to-have-srs-configured/).

## TLS version

There exist email servers in the wild that use old and obsolete TLS protocols like SSLv3, TLSv1 and TLSv1.1.
By default, all these protocols are disabled on the mail server since they are insecure. You can double check
if the mail server is using these old protocols using `nmap --script ssl-enum-ciphers -p 25 <mailserverip>`.

If you want to enable support for these insecure protocols, you can do the following:

* `docker exec -ti mail /bin/bash`
* Edit `/run/haraka/config/tls.ini` and the line `minVersion=TLSv1`. Add this line to the beginning of the file.
* `supervisorctl restart haraka`

Note that the setting is not persistent across mail container and server restarts. So, you have to add this line
by hand, if those events happen.

