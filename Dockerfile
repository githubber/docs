FROM cloudron/base:3.0.0@sha256:455c70428723e3a823198c57472785437eb6eab082e79b3ff04ea584faf46e92

# passed from ./update.sh
ARG MKDOCS_MATERIAL_VERSION
ARG MKDOCS_REDIRECTS_VERSION
ARG SURFER_VERSION

RUN apt-get update && \
    apt install python3-setuptools && \
    pip3 install mkdocs-material==$MKDOCS_MATERIAL_VERSION && \
    pip3 install mkdocs-redirects==${MKDOCS_REDIRECTS_VERSION} && \
    npm i -g redoc-cli && \
    npm install -g cloudron-surfer@$SURFER_VERSION
